/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractValue.h"
#include "Transmission.h"

#include <QString>
#include <QVector>
#include <cassert>
#include <exception>
#include <variant>

namespace qtpdcomtk {

class Process;
class Subscriber;

class ProcessSubscription : public AbstractValue
{
  public:
    ProcessSubscription() = default;

    ProcessSubscription(
            const Transmission::value_type &,
            Process *,
            QString path,
            QVector<uint> selector = {});

    static ProcessSubscription
    event(Process *p, QString path, QVector<uint> selector = {})
    {
        return ProcessSubscription {Transmission::event(), p, path, selector};
    }

    static ProcessSubscription
    poll(double sample_time,
         Process *p,
         QString path,
         QVector<uint> selector = {})
    {
        return ProcessSubscription {
                Transmission::poll(sample_time), p, path, selector};
    }

    static ProcessSubscription
    stream(double sample_time,
           Process *p,
           QString path,
           QVector<uint> selector = {})
    {
        return ProcessSubscription {
                Transmission::stream(sample_time), p, path, selector};
    }

  private:
    struct impl;
    struct poll_impl;
    struct event_impl;
    struct stream_impl;
};

}  // namespace qtpdcomtk
