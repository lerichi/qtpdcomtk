/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractFilter.h"

#include <QString>

namespace qtpdcomtk {

struct DblToQString : ScalarFilter
{
    DblToQString(
            const QString &formatSpec = QString("%1"),
            int precision             = -1,
            char format               = 'f',
            int fieldWidth            = 0,
            QChar fillChar            = QLatin1Char(' ')) :
        AbstractFilter {std::make_unique<impl>()}
    {
        auto _impl        = get_impl<impl>();
        _impl->formatSpec = formatSpec;
        _impl->precision  = precision;
        _impl->fieldWidth = fieldWidth;
        _impl->format     = format;
        _impl->fillChar   = fillChar;
    }

  private:
    struct impl : AbstractFilter::impl
    {
        QString formatSpec;
        int precision;
        int fieldWidth;
        char format;
        QChar fillChar;

        QVariant
        getValue(const std::chrono::nanoseconds &t, AbstractValue &arg) final
        {
            return formatSpec.arg(
                    arg.getValue(t).toDouble(), fieldWidth, format, precision,
                    fillChar);
        }

        bool setValue(const QVariant &val, AbstractValue &arg) final
        {
            return arg.setValue(val.toDouble());
        }
    };
};

}  // namespace qtpdcomtk
