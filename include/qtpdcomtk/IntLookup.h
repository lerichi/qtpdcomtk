/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractFilter.h"

#include <QList>

namespace qtpdcomtk {

template <typename Container>  //
struct IntLookup : ScalarFilter
{
  public:
    using value_type = typename Container::value_type;

    template <typename... Args>  //
    IntLookup(Args &&...args) :
        ScalarFilter {std::make_unique<impl>(std::forward<Args>(args)...)}
    {}

  private:
    struct impl : ScalarFilter::impl
    {
        template <typename... Args>  //
        impl(Args &&...args) : list {std::forward<Args>(args)...}
        {}

        Container list;

        QVariant
        getValue(const std::chrono::nanoseconds &t, AbstractValue &arg) final
        {
            return list.value(arg.getValue(t).toInt());
        }

        bool setValue(const QVariant &val, AbstractValue &arg) final
        {
            return arg.setValue(list.indexOf(val.value<value_type>()));
        }
    };
};

template <typename T>  //
using QListLookup       = IntLookup<QList<T>>;
using QIntListLookup    = QListLookup<int>;
using QStringListLookup = IntLookup<QStringList>;

}  // namespace qtpdcomtk
