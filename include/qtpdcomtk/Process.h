/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QIODevice>
#include <QObject>
#include <QString>
#include <memory>
#include <pdcom5/Process.h>

namespace qtpdcomtk {

namespace _private {
class Process;
}  // namespace _private

class Process : public QObject, public PdCom::Process
{
    Q_OBJECT

  public:
    Process(QObject *parent = nullptr);
    ~Process();

    void setIODevice(QIODevice *);
    QString setMessageFile(QString);

    auto isConnected() const -> bool;

    // Reimplemented from PdCom::Process
    auto hostname() const -> std::string override;
    void listReply(std::vector<PdCom::Variable>, std::vector<std::string>)
            override;
    int read(char *, int) override;
    void write(const char *, size_t) override;
    void flush() override;

  signals:
    void connected() override;  // Reimplemented from PdCom::Process
    void disconnected();
    void protocolError(QString);

  public slots:
    void reset();
    void setLanguage(QString);

  private:
    friend _private::Process;
    const std::shared_ptr<_private::Process> p_impl;
};

}  // namespace qtpdcomtk
