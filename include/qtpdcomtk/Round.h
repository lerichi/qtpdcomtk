/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractFilter.h"

#include <QObject>
#include <cmath>

namespace qtpdcomtk {

class Round : QObject, AbstractFilter<double /*state*/, AbstractValue>
{
    Q_OBJECT

  public:
    Round(double precision, double hysteresis = 2.0 / 3.0) :
        AbstractFilter {std::make_unique<impl>(precision, hysteresis)}
    {}

    AbstractValue operator()(AbstractValue arg) const
    {
        return AbstractFilter::operator()(0.0, std::move(arg));
    }

  public slots:
    void setPrecision(double _precision)
    {
        get_impl<impl>()->precision = _precision;
    }

  private:
    struct impl : AbstractFilter::impl
    {
        impl(double _precision, double _hysteresis) :
            precision {_precision}, hysteresis {_hysteresis}
        {}

        double precision;
        double const hysteresis;

        QVariant getValue(
                const std::chrono::nanoseconds &t,
                double &state,
                AbstractValue &arg) final
        {
            auto value = arg.getValue(t).toDouble() / precision;
            auto diff  = value - state;
            if (diff >= hysteresis or diff < -hysteresis)
                state = std::round(value);
            return state * precision;
        }

        bool setValue(const QVariant &val, double &, AbstractValue &arg) final
        {
            return arg.setValue(val);
        }
    };
};

}  // namespace qtpdcomtk
