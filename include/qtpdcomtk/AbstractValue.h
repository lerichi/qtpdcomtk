/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QModelIndex>
#include <QVariant>
#include <memory>

namespace qtpdcomtk {

struct Subscriber;

/** Base class for all variables */
class AbstractValue
{
  public:
    ~AbstractValue() = default;

    AbstractValue() = default;

    AbstractValue(AbstractValue &&) = default;
    AbstractValue &operator=(AbstractValue &&) = default;

    AbstractValue(const AbstractValue &) = delete;
    AbstractValue &operator=(const AbstractValue &) = delete;

    void enable(bool) const;
    bool isActive() const;
    bool setValue(const QVariant &value) const;
    void setSubscriber(Subscriber *) const;

    QVariant getValue(const std::chrono::nanoseconds &time) const;

  protected:
    struct impl
    {
        virtual ~impl() = default;

        virtual void enable(bool)                = 0;
        virtual bool isActive() const            = 0;
        virtual void setSubscriber(Subscriber *) = 0;

        virtual QVariant getValue(const std::chrono::nanoseconds &) = 0;

        // return true if successful
        virtual bool setValue(const QVariant &) { return false; }
    };
    AbstractValue(std::unique_ptr<impl> impl_) : p_impl {std::move(impl_)} {}

    template <typename T = impl>  //
    T *get_impl() const
    {
        return static_cast<T *>(p_impl.get());
    }

  private:
    std::unique_ptr<impl> p_impl;
};

inline QVariant
AbstractValue::getValue(const std::chrono::nanoseconds &time) const
{
    return p_impl->getValue(time);
}
inline bool AbstractValue::setValue(const QVariant &value) const
{
    return p_impl->setValue(value);
}
inline bool AbstractValue::isActive() const
{
    return p_impl->isActive();
}
inline void AbstractValue::setSubscriber(Subscriber *s) const
{
    p_impl->setSubscriber(s);
}
inline void AbstractValue::enable(bool state) const
{
    p_impl->enable(state);
}

}  // namespace qtpdcomtk
