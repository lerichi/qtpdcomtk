/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "Q_Object.h"

#include <QWidget>

namespace qtpdcomtk {

class Q_Widget : public Q_Object
{
  public:
    Q_Widget(QWidget *parent);

    QWidget *parent();

    virtual void paint(QPainter *, const QRect &, const QPalette &) = 0;
    virtual void
    valueChanged(const std::chrono::nanoseconds &, const QVariant &) = 0;

  private:
    void m_paint();

    // Reimplemented from QObject
    bool eventFilter(QObject *object, QEvent *event) final;

    // Reimplemented from Q_Object
    void valueChanged(const std::chrono::nanoseconds &) final;
};

}  // namespace qtpdcomtk
