/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "overloaded.h"

#include <tuple>    // std::apply
#include <utility>  // std::forward

namespace qtpdcomtk { namespace detail {

/////////////////////////////////////////////////////////////////////////////
// Implementation details
/////////////////////////////////////////////////////////////////////////////
template <typename FilterImpl, typename... Args>  //
struct Filter : AbstractValue
{
    using tuple_type = std::tuple<Args...>;
    static constexpr auto index_sequence =
            std::make_index_sequence<sizeof...(Args)> {};

    Filter(const std::shared_ptr<FilterImpl> &_impl, tuple_type &&val) :
        AbstractValue {std::make_unique<filter_impl>(
                _impl,
                std::forward<tuple_type>(val))}
    {}

    struct filter_impl : AbstractValue::impl
    {
        std::shared_ptr<FilterImpl> filter;
        tuple_type args;

        filter_impl(
                const std::shared_ptr<FilterImpl> &_impl,
                tuple_type &&_args) :
            filter {_impl}, args {std::move(_args)}
        {}

        void enable(bool _state) final
        {
            // see https://stackoverflow.com/a/54053084
            auto func = detail::overloaded {
                    [](auto &) {},
                    [=](AbstractValue &v) { v.enable(_state); }};
            std::apply([=](auto &&..._args) { (func(_args), ...); }, args);
        }
        bool isActive() const final
        {
            auto func = detail::overloaded {
                    [](auto &) { return true; },
                    [](AbstractValue &v) { return v.isActive(); }};
            return std::apply(
                    [=](auto &&..._args) { return (func(_args) && ...); },
                    args);
        }
        void setSubscriber(Subscriber *s) final
        {
            auto func = detail::overloaded {
                    [](auto &) {},
                    [=](AbstractValue &v) { v.setSubscriber(s); }};
            std::apply([=](auto &&..._args) { (func(_args), ...); }, args);
        }

        QVariant getValue(const std::chrono::nanoseconds &t) final
        {
            return getValue(t, index_sequence);
        }

        bool setValue(const QVariant &val) final
        {
            return setValue(val, index_sequence);
        }

      private:
        template <size_t... Is>  //
        QVariant getValue(
                const std::chrono::nanoseconds &t,
                std::index_sequence<Is...>)
        {
            return filter->getValue(t, std::get<Is>(args)...);
        }

        template <size_t... Is>  //
        bool setValue(const QVariant &val, std::index_sequence<Is...>)
        {
            return filter->setValue(val, std::get<Is>(args)...);
        }
    };
};

}}  // namespace qtpdcomtk::detail
