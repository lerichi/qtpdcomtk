/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <cassert>
#include <variant>

namespace qtpdcomtk {

struct poll_t
{
    constexpr poll_t(double ts) : sampleTime {ts} { assert(ts > 0.0); };
    const double sampleTime;
};

struct stream_t
{
    constexpr stream_t(double ts) : sampleTime {ts} { assert(ts >= 0.0); };
    const double sampleTime;
};

constexpr struct event_t
{
} event;

struct Transmission : std::variant<event_t, stream_t, poll_t>
{
    using value_type = std::variant<event_t, stream_t, poll_t>;

    constexpr Transmission() noexcept = default;

    template <typename T>  //
    explicit constexpr Transmission(const T &t) : value_type {t}
    {}

    static constexpr Transmission poll(double interval)
    {
        return Transmission {poll_t {interval}};
    };
    static constexpr Transmission stream(double interval)
    {
        return Transmission {stream_t {interval}};
    };
    static constexpr Transmission event()
    {
        return Transmission {event_t {}};
    }
};

}  // namespace qtpdcomtk
