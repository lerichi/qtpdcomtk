/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractFilter.h"

#include <QCoreApplication>

namespace qtpdcomtk {

struct TranslationFilter : ScalarFilter
{
    TranslationFilter(const char *context = "qtpdcomtk::TranslationFilter") :
        AbstractFilter {std::make_unique<impl>()}
    {
        get_impl<impl>()->context = context;
    }
    TranslationFilter(QObject *translationObject) :
        TranslationFilter {translationObject->metaObject()->className()}
    {}

  private:
    struct impl : AbstractFilter::impl
    {
        const char *context;

        QVariant
        getValue(const std::chrono::nanoseconds &t, AbstractValue &arg) final
        {
            return QCoreApplication::translate(
                    context,
                    arg.getValue(t).toString().toStdString().c_str());
        }
    };
};

}  // namespace qtpdcomtk
