/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "ModelIndexValue.h"
#include "Transmission.h"

#include <QAbstractTableModel>
#include <memory>

class QWidget;

namespace qtpdcomtk {

class Process;

class SubscriptionTableModel : public QAbstractTableModel
{
    Q_OBJECT

  public:
    SubscriptionTableModel(Process *process, const Transmission & = {});
    ~SubscriptionTableModel();

    ModelIndexValue insert(QString, uint col = 0);

    // Reimplemented from QAbstractTableModel
    auto columnCount(const QModelIndex &) const -> int override;
    auto data(const QModelIndex &, int) const -> QVariant override;
    auto flags(const QModelIndex &) const -> Qt::ItemFlags override;
    auto index(int, int, const QModelIndex &) const -> QModelIndex override;
    auto rowCount(const QModelIndex &) const -> int override;

  signals:
    void unknownVariable(QString);

  private:
    struct impl;
    struct poll_impl;
    struct event_impl;
    struct stream_impl;
    const std::unique_ptr<impl> p_impl;

    // Reimplemented from QObject
    bool eventFilter(QObject *obj, QEvent *ev) override;
};

}  // namespace qtpdcomtk
