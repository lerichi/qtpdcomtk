/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "Q_Object.h"

#include <limits>

namespace qtpdcomtk {

class IntVariable : public Q_Object
{
    Q_OBJECT

  public:
    IntVariable(QObject *parent);

    int checkedValue {1};
    int uncheckedValue {0};

    int delta {1};
    int upperLimit {std::numeric_limits<int>::max()};
    int lowerLimit {std::numeric_limits<int>::min()};

    void valueChanged(const std::chrono::nanoseconds &) override;

  signals:
    void valueChanged(int);

  public slots:
    void setValue(int value);

    void setChecked(bool checked);  // Used for checkbox
    void setToCheckedValue();       // Used for pushbutton pressed
    void setToUncheckedValue();     // Used for pushbutton released

    void setCheckedValue(int value);
    void setUncheckedValue(int value);

    void add(int i);       // Add value
    void subtract(int i);  // Subtract value

    void increment();      // Increment by delta
    void decrement();      // Decrement by delta
    void change(bool up);  // Increment when up = true
                           // Decrement when up = false

    void setDelta(int i);  // set increment or decrement value
    void setUpperLimit(int i);
    void setLowerLimit(int i);

  private:
    int m_value;
};

}  // namespace qtpdcomtk
