/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "Transmission.h"

#include <QAbstractTableModel>
#include <QWidget>
#include <memory>

namespace qtpdcomtk {

class SubscriptionListModel : public QAbstractTableModel
{
    Q_OBJECT

  public:
    SubscriptionListModel(Process *process, Transmission = {}) :
        QAbstractTableModel {process}
    {}

    QModelIndex insert(QString, int /*index*/ = 0) { return {}; }

    void enableEventFilter(QWidget *) {}

    // Reimplemented from QAbstractTableModel
    int rowCount(const QModelIndex &) const override { return {}; }
    int columnCount(const QModelIndex &) const override { return {}; }
    QVariant data(const QModelIndex &, int) const override { return {}; }

  private:
    struct impl
    {};
    const std::unique_ptr<impl> p_impl;
};

}  // namespace qtpdcomtk
