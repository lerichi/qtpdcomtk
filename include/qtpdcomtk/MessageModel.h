/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include <QHash>
#include <memory>

namespace qtpdcomtk {

class Process;

namespace _private {
struct MessageModel;
}

class MessageModel : public QAbstractTableModel
{
    Q_OBJECT

    friend _private::MessageModel;

  public:
    MessageModel(QObject *parent = nullptr);
    ~MessageModel();

    void setProcess(qtpdcomtk::Process *);
    void retranslate();

    int rowLimit() const;

    // Reimplemented from QAbstractItemModel
    auto canFetchMore(const QModelIndex & = {}) const -> bool override;
    auto columnCount(const QModelIndex & = {}) const -> int override;
    auto data(const QModelIndex &, int role = Qt::DisplayRole) const
            -> QVariant override;
    auto fetchMore(const QModelIndex & = {}) -> void override;
    auto flags(const QModelIndex & = {}) const -> Qt::ItemFlags override;
    auto headerData(
            int section,
            Qt::Orientation = Qt::Horizontal,
            int role        = Qt::DisplayRole) const -> QVariant override;
    auto rowCount(const QModelIndex & = {}) const -> int override;

  public slots:
    void setRowLimit(int count);

  signals:
    void rowLimitChanged(int);

  private:
    const std::shared_ptr<_private::MessageModel> p_impl;
};

}  // namespace qtpdcomtk
