/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "Q_Widget.h"

namespace qtpdcomtk {

class Led : public Q_Widget
{
  public:
    Led(QWidget *parent, bool invert = false, int dark = 400);

    bool invert;  // false
    int dark;     // For default palette colors (400)

    // Color of led
    QColor onColor;   // Palette::background
    QColor offColor;  // Palette::background.dark

    void paint(QPainter *, const QRect &, const QPalette &) override;

    void
    valueChanged(const std::chrono::nanoseconds &, const QVariant &) override;

  private:
    bool m_state;
};

}  // namespace qtpdcomtk
