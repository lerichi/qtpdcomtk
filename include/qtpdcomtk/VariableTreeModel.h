/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QAbstractItemModel>
#include <memory>

namespace PdCom {
class Variable;
}

namespace qtpdcomtk {

class Process;

namespace _private {
struct VariableTreeModel;
}

class VariableTreeModel : public QAbstractItemModel
{
    Q_OBJECT

    friend _private::VariableTreeModel;

  public:
    VariableTreeModel(Process *, QObject *parent = nullptr);
    ~VariableTreeModel();

    auto getVariable(const QModelIndex &idx) const -> PdCom::Variable;

    // Reimplemented from QAbstractTableModel
    auto canFetchMore(const QModelIndex &) const -> bool override;
    auto columnCount(const QModelIndex &) const -> int override;
    auto data(const QModelIndex &, int) const -> QVariant override;
    auto fetchMore(const QModelIndex &) -> void override;
    auto flags(const QModelIndex &) const -> Qt::ItemFlags override;
    auto hasChildren(const QModelIndex &) const -> bool override;
    auto headerData(int, Qt::Orientation, int role) const
            -> QVariant override;
    auto index(int, int, const QModelIndex &) const -> QModelIndex override;
    auto parent(const QModelIndex &) const -> QModelIndex override;
    auto rowCount(const QModelIndex &) const -> int override;
    auto setData(const QModelIndex &, const QVariant &, int role)
            -> bool override;

  private:
    const std::shared_ptr<_private::VariableTreeModel> p_impl;
};

}  // namespace qtpdcomtk
