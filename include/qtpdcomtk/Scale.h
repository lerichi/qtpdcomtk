/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractFilter.h"

#include <QObject>

namespace qtpdcomtk {

struct Scale : QObject, ScalarFilter
{
    Q_OBJECT

  public:
    Scale(double gain, double offset = 0.0) :
        AbstractFilter {std::make_unique<impl>()}
    {
        auto impl_    = get_impl<impl>();
        impl_->gain   = gain;
        impl_->offset = offset;
    }

  signals:
    void gainChanged(double);
    void offsetChanged(double);

  public slots:
    void setGain(double gain)
    {
        get_impl<impl>()->gain = gain;
        emit gainChanged(gain);
    }
    void setOffset(double offset)
    {
        get_impl<impl>()->offset = offset;
        emit offsetChanged(offset);
    }

  private:
    struct impl : AbstractFilter::impl
    {
        double gain;
        double offset;

        QVariant
        getValue(const std::chrono::nanoseconds &t, AbstractValue &arg) final
        {
            return arg.getValue(t).toDouble() * gain + offset;
        }

        bool setValue(const QVariant &val, AbstractValue &arg) final
        {
            return arg.setValue((val.toDouble() - offset) / gain);
        }
    };
};

}  // namespace qtpdcomtk
