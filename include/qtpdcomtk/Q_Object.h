/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractValue.h"
#include "Subscriber.h"

#include <QObject>
#include <memory>

namespace qtpdcomtk {

class Q_Object : public QObject, public Subscriber
{
    Q_OBJECT

  public:
    Q_Object(QObject *parent) : QObject {parent} {}

    void setVariable(AbstractValue value)
    {
        m_value = std::move(value);
        m_value.setSubscriber(this);
    }

    QVariant getValue(const std::chrono::nanoseconds &t)
    {
        return m_value.getValue(t);
    }

    // Reimplemented from Subscriber
    void stateChanged() override {}

  public slots:
    void setValue(QVariant val) { m_value.setValue(val); }

  private:
    AbstractValue m_value;
};

}  // namespace qtpdcomtk
