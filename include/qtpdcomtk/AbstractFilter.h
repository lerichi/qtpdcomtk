/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "AbstractValue.h"
#include "detail/Filter.h"
#include "detail/overloaded.h"

namespace qtpdcomtk {

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
template <typename... Args>  //
struct AbstractFilter
{
    using tuple_type = std::tuple<Args...>;

    AbstractValue operator()(Args &&...args) const
    {
        return detail::Filter<impl, Args...> {
                p_impl, std::make_tuple(std::move(args)...)};
    }

  protected:
    template <typename Ptr>  //
    AbstractFilter(Ptr _impl) : p_impl {std::move(_impl)}
    {}

    struct impl
    {
        virtual ~impl() {}

        virtual QVariant
        getValue(const std::chrono::nanoseconds &, Args &...) = 0;

        virtual bool setValue(const QVariant &, Args &...) { return false; }
    };

    template <typename T>  //
    std::shared_ptr<T> get_impl()
    {
        return std::dynamic_pointer_cast<T>(p_impl);
    }

  private:
    std::shared_ptr<impl> const p_impl;
};

using ScalarFilter = AbstractFilter<AbstractValue>;

}  // namespace qtpdcomtk
