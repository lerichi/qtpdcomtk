#include "qtpdcomtk/Process.h"
#include "qtpdcomtk/VariableTreeModel.h"

#include <QAbstractItemModelTester>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QShortcut>
#include <QTcpSocket>
#include <QTranslator>
#include <QTreeView>
#include <QUrl>
#include <QVBoxLayout>
#include <QWidget>

struct Translator : QTranslator
{
    using QTranslator::QTranslator;

    QString translate(
            const char *context,
            const char *sourceText,
            const char *disambiguation,
            int n = -1) const override
    {
        qDebug() << __func__ << context << sourceText << disambiguation << n;
        return {};
    }
};

class MainWindow : public QWidget
{
    QDialogButtonBox *buttons;

  public:
    MainWindow(QString url)
    {
        auto socket  = new QTcpSocket {this};
        auto process = new qtpdcomtk::Process {socket};

        auto label    = new QLabel {"URL"};
        auto url_edit = new QLineEdit {url};

        buttons = new QDialogButtonBox {
                QDialogButtonBox::Ok | QDialogButtonBox::Cancel};
        connect(buttons, &QDialogButtonBox::accepted, [url_edit, socket] {
            auto url  = QUrl::fromUserInput(url_edit->text());
            auto host = url.host();
            auto port = url.port(2345);
            socket->disconnectFromHost();
            socket->connectToHost(url.host(), url.port(2345));
        });
        connect(buttons, &QDialogButtonBox::rejected,
                [socket] { socket->disconnectFromHost(); });
        connect(url_edit, &QLineEdit::returnPressed,
                [this] { buttons->button(QDialogButtonBox::Ok)->click(); });

        auto hlayout = new QHBoxLayout;
        hlayout->addWidget(label);
        hlayout->addWidget(url_edit);
        hlayout->addWidget(buttons);

        auto view = new QTreeView;

        auto vlayout = new QVBoxLayout;
        vlayout->addLayout(hlayout);
        vlayout->addWidget(view);

        auto model = new qtpdcomtk::VariableTreeModel(process, view);
        view->setModel(model);
        //        new QAbstractItemModelTester(view->model(),
        //        QAbstractItemModelTester::FailureReportingMode::Warning);
        //        //Fatal);
        view->setHeader(new QHeaderView {Qt::Horizontal});
        view->header()->setSectionResizeMode(
                0, QHeaderView::ResizeToContents);

        setLayout(vlayout);
        setGeometry(0, 0, 600, 600);

        if (not url.isEmpty())
            buttons->button(QDialogButtonBox::Ok)->click();

        auto cancel = new QShortcut(QKeySequence::Cancel, this);
        connect(cancel, &QShortcut::activated,  //
                socket, &QTcpSocket::disconnectFromHost);

        auto quit = new QShortcut(QKeySequence::Quit, this);
        connect(quit, &QShortcut::activated,  //
                qApp, &QApplication::quit);
    }

    ~MainWindow() {}

    void disconnect() { buttons->button(QDialogButtonBox::Cancel)->click(); }
};

int main(int argc, char **argv)
{
    QApplication app {argc, argv};
    QCommandLineParser parser;
    parser.addHelpOption();

    parser.addPositionalArgument(
            "host",
            QCoreApplication::translate("main", "host to connect to"));

    parser.process(app);

    auto args = parser.positionalArguments();

    MainWindow mw {args.isEmpty() ? QString {} : args[0]};

    mw.show();

    return app.exec();
}
