#include "qtpdcomtk/MessageModel.h"
#include "qtpdcomtk/Process.h"

#include <QAbstractItemModelTester>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QDialogButtonBox>
#include <QEvent>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QLocale>
#include <QPushButton>
#include <QShortcut>
#include <QSpinBox>
#include <QTableView>
#include <QTcpSocket>
#include <QTranslator>
#include <QUrl>
#include <QVBoxLayout>
#include <QWidget>
#include <functional>

using namespace std::placeholders;

struct Translator : QTranslator
{
    using QTranslator::QTranslator;

    QString translate(
            const char *context,
            const char *sourceText,
            const char *disambiguation,
            int n = -1) const override
    {
        qDebug() << __func__ << context << sourceText << disambiguation << n;
        return sourceText;
    }

    bool isEmpty() const override { return false; }
};

class MainWindow : public QWidget
{
    QTcpSocket *socket;
    qtpdcomtk::Process *process;
    qtpdcomtk::MessageModel *model;
    Translator translator;
    QDialogButtonBox *buttons;

    void connected()
    {
        qDebug() << __func__ << translator.isEmpty()
                 << qApp->installTranslator(&translator);
    }

    void changeEvent(QEvent *event) override
    {
        if (event->type() == QEvent::LanguageChange)
            qDebug() << __func__;

        QWidget::changeEvent(event);
    }

    bool eventFilter(QObject *obj, QEvent *event) override
    {
        if (event->type() == QEvent::KeyPress) {
            auto keyEvent = static_cast<QKeyEvent *>(event);
            qDebug() << __func__ << keyEvent->key();
            if (keyEvent->key() == Qt::Key_Escape) {
                socket->disconnectFromHost();
                return false;
            }
        }

        return QObject::eventFilter(obj, event);
    }

  public:
    MainWindow(QString url)
    {
        socket  = new QTcpSocket {this};
        process = new qtpdcomtk::Process {socket};

        qDebug() << "Load messages"
                 << process->setMessageFile("messages-abcm_sky.xml");
        process->setLanguage(QLocale {}.bcp47Name());

        auto label        = new QLabel {"URL"};
        auto url_edit     = new QLineEdit {url};
        auto limitSpinBox = new QSpinBox;
        limitSpinBox->setSingleStep(10);

        buttons = new QDialogButtonBox {
                QDialogButtonBox::Ok | QDialogButtonBox::Cancel};
        connect(buttons, &QDialogButtonBox::accepted, [url_edit, this] {
            auto url  = QUrl::fromUserInput(url_edit->text());
            auto host = url.host();
            auto port = url.port(2345);
            socket->disconnectFromHost();
            socket->connectToHost(url.host(), url.port(2345));
        });
        connect(buttons, &QDialogButtonBox::rejected,
                [this] { socket->disconnectFromHost(); });
        connect(url_edit, &QLineEdit::returnPressed,
                [this] { buttons->button(QDialogButtonBox::Ok)->click(); });

        connect(process, &qtpdcomtk::Process::connected,
                [this] { connected(); });

        auto hlayout = new QHBoxLayout;
        hlayout->addWidget(label);
        hlayout->addWidget(url_edit);
        hlayout->addWidget(limitSpinBox);
        hlayout->addWidget(buttons);

        auto view = new QTableView;

        auto vlayout = new QVBoxLayout;
        vlayout->addLayout(hlayout);
        vlayout->addWidget(view);

        model = new qtpdcomtk::MessageModel(view);
        model->setProcess(process);
        model->setObjectName("skydron");
        //        new QAbstractItemModelTester(view->model(),
        //        QAbstractItemModelTester::FailureReportingMode::Warning);
        //        //Fatal);
        limitSpinBox->setValue(model->rowLimit());
        connect(limitSpinBox, &QSpinBox::editingFinished,  //
                [this, limitSpinBox] {
                    model->setRowLimit(limitSpinBox->value());
                });
        connect(model, &qtpdcomtk::MessageModel::rowLimitChanged,  //
                limitSpinBox, &QSpinBox::setValue);

        auto header = new QHeaderView {Qt::Horizontal};
        view->setHorizontalHeader(header);

        header->setSectionResizeMode(0, QHeaderView::Stretch);
        header->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        header->setSectionResizeMode(2, QHeaderView::ResizeToContents);

        setLayout(vlayout);
        setGeometry(0, 0, 600, 600);

        if (not url.isEmpty())
            buttons->button(QDialogButtonBox::Ok)->click();

        auto cancel = new QShortcut(QKeySequence::Cancel, this);
        connect(cancel, &QShortcut::activated,  //
                socket, &QTcpSocket::disconnectFromHost);

        auto quit = new QShortcut(QKeySequence::Quit, this);
        connect(quit, &QShortcut::activated,  //
                qApp, &QApplication::quit);
    }

    ~MainWindow() {}
};

int main(int argc, char **argv)
{
    QApplication app {argc, argv};
    QCommandLineParser parser;
    parser.addHelpOption();

    parser.addPositionalArgument(
            "host",
            QCoreApplication::translate("main", "host to connect to"));

    parser.process(app);

    auto args = parser.positionalArguments();

    MainWindow mw {args.isEmpty() ? QString {} : args[0]};

    mw.show();

    return app.exec();
}
