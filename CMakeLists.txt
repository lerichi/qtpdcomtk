##############################################################################
#
# Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
#
# Derived from http://gitlab.com/etherlab.org/pdcom/CMakeLists.txt Copyright
# (C) 2022 Bjarne von Horn <vh@igh.de> 2022 Florian Pose <fp@igh.de>
#
# This file is part of the QtPdComTk library.
#
# The QtPdComTk library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The QtPdComTk library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdComTk Library. If not, see
# <http://www.gnu.org/licenses/>.
#
##############################################################################

cmake_minimum_required(VERSION 3.16)

project(QtPdComTk
    VERSION 0.0.1
    DESCRIPTION "Toolkit to use PdCom with Qt"
    LANGUAGES CXX)

# Require C++17 for std::variant
# In future, could use concepts, requiring c++20
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if (CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# Configure build

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)
option(BUILD_EXAMPLE "Also build example" OFF)
option(USE_SASL "Use LoginManager with SASL" OFF)
set(QT_VERSION "Qt5" CACHE STRING "Qt version to use (Qt5|Qt6)")
option(ENABLE_TESTING "Test code" OFF)

#configure_file(Doxyfile.in "${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile" @ONLY)

# Check which compiler is being used

set(gcc_like_cxx "$<COMPILE_LANG_AND_ID:CXX,ARMClang,AppleClang,Clang,GNU,LCC>")
set(msvc_cxx "$<COMPILE_LANG_AND_ID:CXX,MSVC>")

# Load required packages

if (USE_SASL)
    find_package(PdCom5 REQUIRED COMPONENTS sasl)
else()
    find_package(PdCom5 REQUIRED)
endif()

# Find QT installation
#set(CMAKE_AUTOMOC ON)
#set(CMAKE_AUTORCC ON)
#set(CMAKE_AUTOUIC ON)

set(PUBLIC_QT_MODULES Widgets Network Test Xml)
if (${ENABLE_TESTING})
    set(EXTRA_QT_MODULES Test)
endif()
find_package(${QT_VERSION} REQUIRED
    COMPONENTS ${PUBLIC_QT_MODULES} ${EXTRA_QT_MODULES} LinguistTools
)

# handle translation files

#set(TS_FILES
#    QtPdComTk_de.ts
#    QtPdComTk_nl.ts
#)

#configure_file(QtPdComTk_ts.qrc "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
#qt5_add_translation(QM_FILES ${TS_FILES})

# add source files and headers

set(PUBLIC_HEADERS
    include/qtpdcomtk/AbstractFilter.h
    include/qtpdcomtk/AbstractValue.h
    include/qtpdcomtk/BarGraph.h
    include/qtpdcomtk/DblToQString.h
    include/qtpdcomtk/IntLookup.h
    include/qtpdcomtk/IntVariable.h
    include/qtpdcomtk/Led.h
    include/qtpdcomtk/MessageModel.h
    include/qtpdcomtk/ModelIndexValue.h
    include/qtpdcomtk/Process.h
    include/qtpdcomtk/ProcessSubscription.h
    include/qtpdcomtk/Q_ButtonGroup.h
    include/qtpdcomtk/Q_Object.h
    include/qtpdcomtk/Q_Widget.h
    include/qtpdcomtk/Round.h
    include/qtpdcomtk/Scale.h
    include/qtpdcomtk/SetQObjectProperty.h
    include/qtpdcomtk/Subscriber.h
    include/qtpdcomtk/SubscriptionTableModel.h
    include/qtpdcomtk/TranslationFilter.h
    include/qtpdcomtk/Transmission.h
    include/qtpdcomtk/VariableTreeModel.h
)

set(SOURCES
    src/BarGraph.cpp
    src/IntVariable.cpp
    src/Led.cpp
    src/MessageModel.cpp
    src/ModelIndexValue.cpp
    src/Process.cpp
    src/ProcessSubscription.cpp
    src/Q_ButtonGroup.cpp
    src/Q_Widget.cpp
    src/SubscriptionTableModel.cpp
    src/VariableTreeModel.cpp
    src/private/MessageManager.h        src/private/MessageManager.cpp
    src/private/MessageModel.h          src/private/MessageModel.cpp
    src/private/PdComTypeInfo.h
    src/private/Process.h               src/private/Process.cpp
    src/private/ProcessSubscription.h
    src/private/Variable.h              src/private/Variable.cpp
    src/private/VariableTreeModel.h     src/private/VariableTreeModel.cpp
)
#qt_add_big_resources(SOURCES src/resource.qrc)

if (USE_SASL)
    list(APPEND PUBLIC_HEADERS "${PROJECT_NAME}/LoginManager.h")
    list(APPEND SOURCES "src/LoginManager.cpp")
endif()

string(TOLOWER "${QT_VERSION}pdcomtk" LIB_NAME)
add_library(${LIB_NAME}
    ${PUBLIC_HEADERS}
    ${SOURCES}

    ${QM_FILES}
    #${CMAKE_CURRENT_BINARY_DIR}/QtPdComTk_ts.qrc
)
set_target_properties(${LIB_NAME} PROPERTIES
    POSITION_INDEPENDENT_CODE ${BUILD_SHARED_LIBS})

# Set compiler warning flags

target_compile_options(${LIB_NAME} PRIVATE
    "$<${gcc_like_cxx}:$<BUILD_INTERFACE:-Wall;-Wextra;-Wshadow;-Wformat=2;-Wunused>>"
    "$<${msvc_cxx}:$<BUILD_INTERFACE:-W3>>"
    )


#configure_file(QtPdComTk.h.in "${CMAKE_CURRENT_BINARY_DIR}/QtPdComTk.h")

# set include path

target_include_directories(${LIB_NAME} PRIVATE include
    PUBLIC
    #"$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}>"
    #"$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
    #"$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>"
    #"$<INSTALL_INTERFACE:include>"
)

# link to required and optional libraries

set(_PUBLIC_QT_MODULES ${PUBLIC_QT_MODULES})
list(TRANSFORM _PUBLIC_QT_MODULES PREPEND "Qt5::")
target_link_libraries(${LIB_NAME} PUBLIC
    EtherLab::pdcom5
    ${_PUBLIC_QT_MODULES}
  PRIVATE
  #Qt5::Xml
)

if (WIN32)
    # for gethostname in Process.cpp
    target_link_libraries(${LIB_NAME} PRIVATE -lwsock32)
endif()

# use sasl?

if (USE_SASL)
    target_link_libraries(${LIB_NAME} PRIVATE
        EtherLab::pdcom5-sasl
    )
    target_compile_definitions(${LIB_NAME} PRIVATE PDCOM_USE_SASL)
endif()

# bake in version information

if (VERSION_HASH)
    # Hash was given as option, so write it directly
    # useful for OBS packaging
    file (WRITE "${CMAKE_CURRENT_BINARY_DIR}/git_revision_hash.h"
        "#define GIT_REV \"${VERSION_HASH}\"
")
else()
    # recompute hash on every `make` invocation
    # Note: CMake variable definitions (-D) must be before the -P option!
    add_custom_target (GitRevision
        BYPRODUCTS "${CMAKE_CURRENT_BINARY_DIR}/git_revision_hash.h"
        COMMAND ${CMAKE_COMMAND}
            -DSOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}"
            -DHASH_MACRO_NAME="GIT_REV"
            -DTARGET_FILE="${CMAKE_CURRENT_BINARY_DIR}/git_revision_hash.h"
            -P ${CMAKE_CURRENT_SOURCE_DIR}/git_revision.cmake
    )
add_dependencies(${LIB_NAME} GitRevision)
endif()

# enable resource compiler etc.

set_target_properties(${LIB_NAME} PROPERTIES
    AUTOUIC ON
    AUTOMOC ON
    AUTORCC ON
    PUBLIC_HEADER "${PUBLIC_HEADERS}"
    #CXX_VISIBILITY_PRESET "hidden"
    #VISIBILITY_INLINES_HIDDEN 1
    VERSION ${PROJECT_VERSION}
    SOVERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}"
)

#target_compile_features(${LIB_NAME} PUBLIC cxx_std_11)
# install library and headers

install(TARGETS ${LIB_NAME}
    EXPORT ${LIB_NAME}Targets
    ARCHIVE DESTINATION       "${CMAKE_INSTALL_LIBDIR}"
    LIBRARY DESTINATION       "${CMAKE_INSTALL_LIBDIR}"
    RUNTIME DESTINATION       "${CMAKE_INSTALL_BINDIR}"
    PUBLIC_HEADER DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${LIB_NAME}"
)

file(GLOB DETAIL_HEADERS "include/qtpdcomtk/detail/*.h")
install(FILES ${DETAIL_HEADERS}
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${LIB_NAME}/detail"
)

#install(FILES "${CMAKE_CURRENT_BINARY_DIR}/QtPdComTk.h"
#DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
#)

# export target for use in other projects

set(ConfigPackageLocation "${CMAKE_INSTALL_LIBDIR}/cmake/${LIB_NAME}")
configure_package_config_file(Config.cmake.in
    "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Config.cmake"
    INSTALL_DESTINATION ${ConfigPackageLocation}
)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

install(EXPORT ${LIB_NAME}Targets
    NAMESPACE EtherLab::
    FILE "${LIB_NAME}Targets.cmake"
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${LIB_NAME}"
)
install(FILES
    "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Config.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}ConfigVersion.cmake"
    DESTINATION ${ConfigPackageLocation}
)
export(EXPORT ${LIB_NAME}Targets
    FILE "${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Targets.cmake"
    NAMESPACE EtherLab::
)

# build example

if (BUILD_EXAMPLE)
    #add_library(EtherLab::${LIB_NAME} ALIAS ${LIB_NAME})
    add_subdirectory(example)
endif()

# Configure testing

if (ENABLE_TESTING)
    # 'https://www.youtube.com/watch?v=pxJoVRfpRPE' 
    include(CTest)
    add_subdirectory(test)
endif()
