/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "qtpdcomtk/Process.h"
#include "qtpdcomtk/Subscriber.h"

#include <QCoreApplication>
#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace qtpdcomtk;
using testing::_;
using testing::ReturnArg;

namespace {

struct ProcessTest : testing::Test, Subscriber
{
    int argc {0};

    QCoreApplication app {argc, nullptr};
    QTcpSocket socket {&app};
    Process process {&socket};

    void SetUp() override
    {
        QObject::connect(
                &socket, &QTcpSocket::stateChanged,
                [](QAbstractSocket::SocketState s) { qDebug() << s; });

        QObject::connect(&process, &Process::connected, [] {
            qDebug() << "connected";
        });
        QObject::connect(&process, &Process::disconnected, [] {
            qDebug() << "disconnected";
        });

        socket.connectToHost("localhost", 2345);
    }

    void stateChanged() override {}
    void valueChanged(const std::chrono::nanoseconds &) {}
};

}  // namespace

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
TEST_F(ProcessTest, basic)
{
    QTimer::singleShot(3000, &app, &QCoreApplication::quit);

    app.exec();
}
