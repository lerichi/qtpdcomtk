/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "qtpdcomtk/IntLookup.h"
#include "qtpdcomtk/Process.h"
#include "qtpdcomtk/ProcessSubscription.h"
#include "qtpdcomtk/Round.h"
#include "qtpdcomtk/Scale.h"
#include "qtpdcomtk/Subscriber.h"
#include "qtpdcomtk/TranslationFilter.h"

#include <QCoreApplication>
#include <QTcpSocket>
#include <QTimer>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace qtpdcomtk;
using testing::_;
using testing::ReturnArg;

namespace {

struct SubscriptionTest : testing::Test, Subscriber
{
    int argc {0};

    QCoreApplication app {argc, nullptr};
    QTcpSocket socket {&app};
    Process process {&socket};
    AbstractValue value, value1;

    void SetUp() override
    {
        QObject::connect(&process, &Process::connected, [] {
            qDebug() << "connected";
        });
        QObject::connect(&process, &Process::disconnected, [] {
            qDebug() << "disconnected";
        });

        socket.connectToHost("localhost", 2345);
    }

    void stateChanged() override { qDebug() << __func__ << value.isActive(); }
    void valueChanged(const std::chrono::nanoseconds &t)
    {
        qDebug() << __func__ << value.getValue(t);
    }
};

}  // namespace

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
TEST_F(SubscriptionTest, basic)
{
    QTimer::singleShot(3000, &app, &QCoreApplication::quit);

    value = Round {0.01}(ProcessSubscription {
            Transmission::poll(0.5), &process, "/VCO/cos"});
    value.setSubscriber(this);

    app.exec();
}

TEST_F(SubscriptionTest, event)
{
    QTimer::singleShot(3000, &app, &QCoreApplication::quit);

    value = QIntListLookup {1, 2, 3}(ProcessSubscription {
            Transmission::event(), &process, "/VCO/cos"});
    value.setSubscriber(this);

    app.exec();
}

TEST_F(SubscriptionTest, stream)
{
    QTimer::singleShot(3000, &app, &QCoreApplication::quit);

    value = Scale {100, -10}(ProcessSubscription {
            Transmission::stream(0.5), &process, "/VCO/cos"});
    value.setSubscriber(this);
    value1 = TranslationFilter {&app}(ProcessSubscription {
            Transmission::stream(0.4), &process, "/VCO/sin"});
    value1.setSubscriber(this);

    app.exec();
}
