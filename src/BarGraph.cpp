/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/BarGraph.h"

#include <QDebug>
#include <QPainter>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
BarGraph::BarGraph(
        QWidget *parent,
        Qt::Orientation _orientation,
        double _max,
        double _min) :
    Q_Widget {parent}, max {_max}, min {_min}, orientation {_orientation}
{}

/////////////////////////////////////////////////////////////////////////////
void BarGraph::paint(
        QPainter *painter,
        const QRect &rect,
        const QPalette &palette)
{
    int width  = rect.width();
    int height = rect.height();

    painter->setPen(Qt::transparent);

    // Clear
    painter->fillRect(0, 0, width, height, palette.window());

    if (orientation == Qt::Horizontal) {
        painter->scale(width / (max - min), height);
        painter->translate(-min, 0);
        painter->fillRect(QRectF {0, 0, m_value, 1}, palette.windowText());
    }
    else {
        painter->scale(width, height / (min - max));
        painter->translate(0, -max);
        painter->fillRect(QRectF {0, 0, 1, m_value}, palette.windowText());
    }
}

/////////////////////////////////////////////////////////////////////////////
void BarGraph::valueChanged(
        const std::chrono::nanoseconds &,
        const QVariant &val)
{
    m_value = val.toDouble();
}
