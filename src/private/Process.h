/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "../include/qtpdcomtk/Process.h"

#include <QAbstractSocket>
#include <QDomDocument>
#include <QMetaObject>
#include <QString>
#include <memory>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Subscriber.h>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

namespace PdCom {
class Variable;
class Subscription;
class MessageManagerBase;
}  // namespace PdCom

namespace qtpdcomtk {

class Subscriber;

namespace _private {

struct MessageManager;
struct ProcessSubscription;
struct VariableTreeModel;

class Process : public std::enable_shared_from_this<Process>
{
    friend qtpdcomtk::Process;

    struct Private;

  public:
    Process(qtpdcomtk::Process *parent, Private);
    ~Process();

    qtpdcomtk::Process *const parent;

    void setIODevice(QIODevice *);
    void setMessageFile(QDomDocument);
    void setLanguage(QString);

    QIODevice *ioDevice() const;
    QAbstractSocket *abstractSocket() const;
    void disconnectFromHost();

    using weak_ptr_t = std::weak_ptr<Process>;

    static weak_ptr_t weak_ptr(qtpdcomtk::Process *p);
    static bool is_connected(const weak_ptr_t &p);
    static void
    list(const weak_ptr_t &,
         const std::weak_ptr<VariableTreeModel> &,
         const QString &);

    static std::shared_ptr<PdCom::Subscriber>
    getSubscriber(qtpdcomtk::Process *, double);

    static std::shared_ptr<MessageManager>
    getMessageManager(qtpdcomtk::Process *);
    static void
    setMessageManager(const weak_ptr_t &, PdCom::MessageManagerBase *);

    static QString
    messageToolTip(const weak_ptr_t &, const std::string &path, int index);
    static QString messageText(
            const weak_ptr_t &,
            const std::string &path,
            int index,
            const std::string &text);

  private:
    QIODevice *m_iodevice {nullptr};

    QMetaObject::Connection connection;
    std::queue<std::weak_ptr<VariableTreeModel>> m_list_queue;
    std::weak_ptr<MessageManager> m_messageManager;

    static std::shared_ptr<Process> create(qtpdcomtk::Process *parent);
    std::string outcache;
    bool connected {false};

    void m_connected();
    void m_disconnected();
    void m_readyRead();
    QDomDocument m_messages;
    QString m_language;

    void listReply(
            std::vector<PdCom::Variable> &variables,
            std::vector<std::string> &dirs);

    struct MultiSubscriber;
    std::unordered_map<
            double,  // sampleTime
            std::weak_ptr<MultiSubscriber>>
            m_subscriberMap;
};

}  // namespace _private
}  // namespace qtpdcomtk
