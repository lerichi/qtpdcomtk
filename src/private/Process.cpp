/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Process.h"

#include "MessageManager.h"
#include "ProcessSubscription.h"
#include "VariableTreeModel.h"

// Other library includes
#include "qtpdcomtk/Process.h"
#include "qtpdcomtk/Subscriber.h"

// System includes
#include <cmath>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

using namespace qtpdcomtk::_private;

namespace {

/////////////////////////////////////////////////////////////////////////////
QString format_path(const std::string &path, int index)
{
    auto str = QString::fromStdString(path);

    if (index >= 0)
        str.append('[' + QString::number(index) + ']');

    return str;
}

/////////////////////////////////////////////////////////////////////////////
QDomElement
find_language_element(QDomNode node, QString tag_name, QString lang)
{
    static const auto lang_attribute = QString {"lang"};

    QDomElement default_element;

    auto children = node.childNodes();

    for (int i = 0; i < children.size(); ++i) {
        auto element = children.at(i).toElement();
        if (element.tagName() == tag_name) {
            auto attribute = element.attribute(lang_attribute);
            if (attribute == lang)
                return element;

            if (not element.hasAttribute(lang_attribute)
                or attribute.isEmpty() or default_element.isNull())
                default_element = element;
        }
    }

    return default_element;
}

/////////////////////////////////////////////////////////////////////////////
QString get_message_tooltip(QDomNode node, QString lang)
{
    return find_language_element(node, "Description", lang).text();
}

/////////////////////////////////////////////////////////////////////////////
QString format_array(QDomElement element, int index, QString lang)
{
    // <Array>
    //   <Entry>
    //     <Text lang="de>...</Text>
    //     <Text lang="en>...</Text>
    //   </Entry>
    //   <!-- more entries -->
    // </Array>
    auto children = element.childNodes();
    for (auto i = 0; i < children.size(); ++i) {
        element = children.at(i).toElement();
        if (element.tagName() == "Entry" and index-- == 0)
            return find_language_element(element, "Text", lang).text();
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////////
QString format_map(QDomElement element, int index, QString lang)
{
    // <Map key="name"/>
    auto key = element.attribute("key");

    // find a matching <Array key="$key"></Array> in the parents
    for (auto node = element.parentNode(); not node.isNull();
         node      = node.parentNode()) {
        auto children = node.childNodes();
        for (auto i = 0; i < children.size(); ++i) {
            element = children.at(i).toElement();
            if (element.tagName() == "Array"
                and element.attribute("key") == key)
                return format_array(element, index, lang);
        }
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////////
QString format_index(QDomElement element, int index, QString)
{
    // <Index offset="8"/>
    return QString::number(index + element.attribute("offset").toInt());
}

/////////////////////////////////////////////////////////////////////////////
QString get_message_text(QDomNode node, QString lang, int index)
{
    QString text;

    auto children = find_language_element(node, "Text", lang).childNodes();

    for (auto i = 0; i < children.size(); ++i) {
        node = children.at(i);

        switch (node.nodeType()) {
            case QDomNode::TextNode:
                text.append(node.nodeValue());
                break;

            case QDomNode::ElementNode: {
                auto element = node.toElement();
                using fn_ptr = QString (*)(QDomElement, int, QString);

                static const auto map = std::unordered_map<QString, fn_ptr> {
                        {"Map", format_map},
                        {"Index", format_index},
                        {"Array", format_array},
                };

                auto it = map.find(element.tagName());
                if (it != map.end())
                    text.append(
                            std::invoke(it->second, element, index, lang));
            }

            default:
                break;
        }
    }

    return text;
}

/////////////////////////////////////////////////////////////////////////////
// return name of NodeGroup
QString get_nodegroup_name(QDomElement element)
{
    static const auto name_attribute = QString {"name"};

    // <NodeGroup name="...">
    if (element.hasAttribute(name_attribute))
        return element.attribute(name_attribute);

    auto children = element.childNodes();

    // search child elements for
    // <Node name="...">
    // or <Node>...</Node>
    for (int i = 0; i < children.size(); ++i) {
        element = children.at(i).toElement();
        if (element.tagName() == "Node") {
            return element.hasAttribute(name_attribute)
                    ? element.attribute(name_attribute)
                    : element.text();
        }
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////////
// search for child element having
//          <NodeGroup name="...">  <!-- preferred -->
//            ...
//          </NodeGroup>
// or
//          <NodeGroup>
//            ...
//            <Node name="..."/>
//            ...
//          </NodeGroup>
// or
//          <NodeGroup>
//            ...
//            <Node>...</Node>
//            ...
//          </NodeGroup>
QDomElement find_nodegroup(QDomElement element, QString name)
{
    auto children = element.childNodes();

    for (int i = 0; i < children.size(); ++i) {
        element = children.at(i).toElement();
        if (element.tagName() == "NodeGroup"
            and get_nodegroup_name(element) == name)
            return element;
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////////
QDomElement find_path_element(QDomElement node, const std::string &path)
{
    for (auto &dir :
         QString::fromStdString(path).split('/', Qt::SkipEmptyParts))
        node = find_nodegroup(node, dir);

    return node;
}

}  // namespace

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct Process::MultiSubscriber :
    PdCom::Subscriber,
    std::enable_shared_from_this<MultiSubscriber>
{
  private:
    // clang-format off
    struct Private { explicit Private() {} };
    // clang-format on

  public:
    MultiSubscriber(
            const PdCom::Transmission &td,
            std::shared_ptr<MultiSubscriber> parent,
            Private) :
        PdCom::Subscriber {td}, m_parent {parent}
    {}

    static std::shared_ptr<MultiSubscriber> create(double ts)
    {
        return std::make_shared<MultiSubscriber>(
                PdCom::Transmission::fromDouble(ts), nullptr, Private {});
    }

    void stateChanged(const PdCom::Subscription &s) final
    {
        auto *subscription = static_cast<ProcessSubscription *>(
                const_cast<PdCom::Subscription *>(&s));
        auto *subscriber = subscription->subscriber();

        if (s.getState() == PdCom::Subscription::State::Active) {
            auto task_id = s.getVariable().getTaskId();

            // Make sure task id's match
            if (m_task_id < 0)
                m_task_id = task_id;
            else if (task_id != m_task_id) {
                subscription->set_subscriber(
                        m_parent->m_get_subscriber(task_id));
                subscription->subscribe();
                return;
            }

            m_activeSet[subscriber].insert(subscription);
        }
        else {
            // Remove subscriber from active set

            auto it = m_activeSet.find(subscriber);

            if (it != m_activeSet.end()) {
                auto &set = it->second;
                set.erase(subscription);
                if (set.empty())
                    m_activeSet.erase(it);
            }
        }

        subscription->stateChanged();
    }

    void newValues(std::chrono::nanoseconds ts) final
    {
        for (auto &[k, v] : m_activeSet)
            k->valueChanged(ts);
    }

  private:
    const std::shared_ptr<MultiSubscriber> m_parent;

    int m_task_id {-1};
    std::unordered_map<int, std::weak_ptr<MultiSubscriber>>
            m_secondary_subscriber;
    std::unordered_map<
            qtpdcomtk::Subscriber *,
            std::unordered_set<ProcessSubscription *>>
            m_activeSet;

    std::shared_ptr<MultiSubscriber> m_get_subscriber(int task_id)
    {
        auto self = shared_from_this();
        if (m_task_id == task_id)
            return self;

        auto &weak      = m_secondary_subscriber[task_id];
        auto subscriber = weak.lock();
        if (not subscriber) {
            subscriber = std::make_shared<MultiSubscriber>(
                    getTransmission(), std::move(self), Private {});
            weak = subscriber;
        }

        return subscriber;
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct Process::Private
{};

/////////////////////////////////////////////////////////////////////////////
Process::Process(qtpdcomtk::Process *p, Private) : parent {p}
{
    setIODevice(qobject_cast<QIODevice *>(p->parent()));
}

/////////////////////////////////////////////////////////////////////////////
Process::~Process()
{
    setIODevice(nullptr);
}

/////////////////////////////////////////////////////////////////////////////
void Process::setIODevice(QIODevice *device)
{
    QObject::disconnect(connection);
    if (m_iodevice)
        QObject::disconnect(m_iodevice, nullptr, parent, nullptr);

    m_iodevice = device;

    if (not device)
        return;

    connection = QObject::connect(
            device, &QIODevice::readyRead,  //
            [this] { m_readyRead(); });

    if (auto *socket = abstractSocket())
        QObject::connect(
                socket, &QAbstractSocket::disconnected,  //
                parent, &qtpdcomtk::Process::reset);
}

/////////////////////////////////////////////////////////////////////////////
QIODevice *Process::ioDevice() const
{
    return m_iodevice;
}

/////////////////////////////////////////////////////////////////////////////
void Process::setMessageFile(QDomDocument doc)
{
    m_messages = doc;
}

/////////////////////////////////////////////////////////////////////////////
void Process::setLanguage(QString lang)
{
    m_language = lang;
}

/////////////////////////////////////////////////////////////////////////////
QAbstractSocket *Process::abstractSocket() const
{
    return qobject_cast<QAbstractSocket *>(m_iodevice);
}

/////////////////////////////////////////////////////////////////////////////
void Process::disconnectFromHost()
{
    if (auto *socket = abstractSocket())
        socket->disconnectFromHost();
}

/////////////////////////////////////////////////////////////////////////////
void Process::m_readyRead()
{
    try {
        while (not m_iodevice->atEnd())
            parent->asyncData();

        return;
    }
    catch (const std::exception &e) {
        emit parent->protocolError(  //
                parent
                        ->tr(QT_TRANSLATE_NOOP(
                                "qtpdcomtk::Process", "Protocol error: %1"))
                        .arg(e.what()));
    }
    catch (...) {
        emit parent->protocolError(parent->tr(QT_TRANSLATE_NOOP(
                "qtpdcomtk::Process", "Unknown protocol error.")));
    }

    disconnectFromHost();
}

/////////////////////////////////////////////////////////////////////////////
std::shared_ptr<Process> Process::create(qtpdcomtk::Process *p)
{
    return std::make_shared<Process>(p, Private {});
}

/////////////////////////////////////////////////////////////////////////////
Process::weak_ptr_t Process::weak_ptr(qtpdcomtk::Process *p)
{
    return p->p_impl->weak_from_this();
}

/////////////////////////////////////////////////////////////////////////////
bool Process::is_connected(const weak_ptr_t &p)
{
    auto self = p.lock();
    return self and self->connected;
}

/////////////////////////////////////////////////////////////////////////////
void Process::m_connected()
{
    connected = true;
}

/////////////////////////////////////////////////////////////////////////////
void Process::m_disconnected()
{
    connected = false;
    outcache.clear();
    m_list_queue = {};
}

/////////////////////////////////////////////////////////////////////////////
void Process::list(
        const weak_ptr_t &process,
        const std::weak_ptr<VariableTreeModel> &model,
        const QString &path)
{
    if (auto self = process.lock()) {
        self->m_list_queue.push(model);
        self->parent->list(path.toStdString());
    }
}

/////////////////////////////////////////////////////////////////////////////
void Process::listReply(
        std::vector<PdCom::Variable> &variables,
        std::vector<std::string> &dirs)
{
    if (auto model = m_list_queue.front().lock())
        model->m_insert(variables, dirs);
    m_list_queue.pop();
}

/////////////////////////////////////////////////////////////////////////////
std::shared_ptr<MessageManager>
Process::getMessageManager(qtpdcomtk::Process *p)
{
    auto &self = p->p_impl;

    auto manager = self->m_messageManager.lock();
    if (not manager)
        self->m_messageManager = manager =
                MessageManager::create(self->weak_from_this());

    return manager;
}

/////////////////////////////////////////////////////////////////////////////
void Process::setMessageManager(
        const weak_ptr_t &process,
        PdCom::MessageManagerBase *mm)
{
    if (auto self = process.lock())
        self->parent->setMessageManager(mm);
}

/////////////////////////////////////////////////////////////////////////////
std::shared_ptr<PdCom::Subscriber>
Process::getSubscriber(qtpdcomtk::Process *process, double ts)
{
    auto &self      = process->p_impl;
    auto &weak_ptr  = self->m_subscriberMap[ts];
    auto subscriber = weak_ptr.lock();
    if (not subscriber) {
        subscriber = MultiSubscriber::create(ts);
        weak_ptr   = subscriber;
    }

    return subscriber;
}

/////////////////////////////////////////////////////////////////////////////
QString Process::messageToolTip(
        const weak_ptr_t &p,
        const std::string &path,
        int index)
{
    QString tool_tip;
    if (auto self = p.lock())
        tool_tip = get_message_tooltip(
                           find_path_element(
                                   self->m_messages.documentElement(), path),
                           self->m_language)
                + "\n\n" + format_path(path, index);

    return tool_tip;
}

/////////////////////////////////////////////////////////////////////////////
QString Process::messageText(
        const weak_ptr_t &p,
        const std::string &path,
        int index,
        const std::string &default_text)
{
    QString message;
    if (auto self = p.lock()) {
        message = get_message_text(
                find_path_element(
                        self->m_messages.documentElement(), path),  //
                self->m_language, index);

        if (message.isEmpty() and not default_text.empty())
            message = self->parent->tr(
                    default_text.c_str(), path.c_str(), index);
    }

    if (message.isEmpty())
        message = default_text.empty() ? format_path(path, index)
                                       : QString::fromStdString(default_text);

    return message;
}
