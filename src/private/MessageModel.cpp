/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModel.h"

#include "MessageManager.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDomDocument>
#include <QFile>
#include <QLocale>
#include <functional>  // std::bind
#include <limits>      // std::numeric_limits::max

using namespace qtpdcomtk::_private;
using namespace std::placeholders;

struct MessageModel::Private
{};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
namespace {
/////////////////////////////////////////////////////////////////////////////
QDateTime make_time(const PdCom::Message &m)
{
    return QDateTime::fromMSecsSinceEpoch(
            std::chrono::duration_cast<std::chrono::milliseconds>(m.time)
                    .count());
}

/////////////////////////////////////////////////////////////////////////////
QVariant format_date(const QDateTime &date)
{
    if (date.isNull())
        return {};

    if (date.date().addDays(1) > QDateTime::currentDateTime().date())
        return date.time().toString("hh:mm:ss.zzz");

    return date.toString("yyyy-MM-dd hh:mm:ss");
}

/////////////////////////////////////////////////////////////////////////////
bool is_reset(const PdCom::Message &m)
{
    return m.level == PdCom::LogLevel::Reset;
}

/////////////////////////////////////////////////////////////////////////////
[[maybe_unused]] QDebug debug(const char *func, const PdCom::Message &m)
{
    auto dbg = qDebug();
    dbg << func << "message" << int(m.level) << m.seqNo
        << QString::fromStdString(m.path) << m.index
        << QString::fromStdString(m.text);
    return dbg;
}

using SeqNo_t = decltype(PdCom::Message::seqNo);
constexpr inline bool seqNo_less(SeqNo_t a, SeqNo_t b)
{
#if 1
    using SSegNo_t = std::make_signed<SeqNo_t>::type;
    return static_cast<SSegNo_t>(b - a) > 0;
#else
    constexpr auto mid = std::numeric_limits<SeqNo_t>::max() / 2 + 1;
    return b - a - 1 < mid;  // this is more math?
#endif
}
static_assert(seqNo_less(2, 3));
static_assert(not seqNo_less(2, 2));
static_assert(not seqNo_less(3, 2));

static_assert(not seqNo_less(3, -2));
static_assert(seqNo_less(-2, 3));

static_assert(not seqNo_less(-2, -3));
static_assert(not seqNo_less(-2, -2));
static_assert(seqNo_less(-2, -1));

}  // namespace

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
MessageModel::Message::Message(const PdCom::Message &m) :
    path {m.path}, index {m.index}
{
    setSeqNo = m.seqNo;
}
bool MessageModel::Message::operator<(const PdCom::Message &m) const
{
    return path < m.path or (path == m.path and index < m.index);
}
bool MessageModel::Message::operator==(const PdCom::Message &m) const
{
    return path == m.path and index == m.index;
}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
MessageModel::MessageItem::MessageItem(
        Message *m,
        const PdCom::Message &pdcom) :
    message {m}, seqNo {pdcom.seqNo}, setTime {make_time(pdcom)}

{}
bool MessageModel::MessageItem::operator<(SeqNo_t other) const
{
    return seqNo_less(other, seqNo);  // Higher seqNo come first
}
bool MessageModel::MessageItem::operator==(SeqNo_t other) const
{
    return seqNo == other;
}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
MessageModel::MessageModel(qtpdcomtk::MessageModel *m, Private) : m_parent {m}
{
    m_canFetchMore = false;
    m_max_rows     = 0;

    setRowLimit(20);
}

/////////////////////////////////////////////////////////////////////////
MessageModel::~MessageModel()
{
    if (m_messageManager)
        m_messageManager->remove(this);

    if (auto process = m_process.lock()) {
        QObject::disconnect(m_process_connection);
        QObject::disconnect(m_process_disconnection);
    }
}

/////////////////////////////////////////////////////////////////////////
std::shared_ptr<MessageModel> MessageModel::create(qtpdcomtk::MessageModel *m)
{
    return std::make_shared<MessageModel>(m, Private {});
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::retranslate()
{
    for (auto &m : m_messages)
        m.text = Process::messageText(
                m_process, m.path, m.index, m.original_text);

    if (not m_message_items.empty())
        emit m_parent->dataChanged(
                m_parent->createIndex(0, 0),               //
                m_parent->createIndex(rowCount() - 1, 0),  //
                {Qt::DisplayRole});
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::setProcess(qtpdcomtk::Process *p)
{
    m_process        = Process::weak_ptr(p);
    m_messageManager = Process::getMessageManager(p);
    m_messageManager->add(this);

    m_process_connection = QObject::connect(
            p, &qtpdcomtk::Process::connected,
            std::bind(&MessageModel::m_connected, this));
    m_process_disconnection = QObject::connect(
            p, &qtpdcomtk::Process::disconnected,
            std::bind(&MessageModel::m_disconnected, this));

    if (p->isConnected())
        m_connected();
    else
        m_disconnected();
}

///////////////////////////////////////////////////////////////////////////
void MessageModel::setRowLimit(size_t count)
{
    auto old_max_rows = m_max_rows;

    if (m_max_rows != count) {
        m_max_rows = count;
        emit m_parent->rowLimitChanged(count);
    }

    if (m_max_rows >= old_max_rows) {
        if (rowCount() >= old_max_rows)
            fetchMore();
        return;
    }

    count = std::distance(m_message_items.begin(), m_firstResetRow());
    auto delete_from_row = std::max<size_t>(m_max_rows, count);

    count = rowCount();
    if (delete_from_row >= count)
        return;

    // Copy reset time back to the messages
    std::for_each_n(
            m_message_items.rbegin(), count - delete_from_row,
            [](auto &item) { item.message->resetTime = item.resetTime; });

    auto it        = std::next(m_message_items.begin(), delete_from_row);
    m_seqNo        = it->seqNo;
    m_canFetchMore = true;

    m_parent->beginRemoveRows({}, delete_from_row, count - 1);
    //    qDebug() << __func__ << "remove" << delete_from_row << (count - 1);
    m_message_items.erase(it, m_message_items.end());
    m_parent->endRemoveRows();
}

/////////////////////////////////////////////////////////////////////////
Qt::ItemFlags MessageModel::flags(const QModelIndex &idx) const
{
    return Process::is_connected(m_process)
                    and m_getItem(idx)->resetTime.isNull()
            ? Qt::ItemIsEnabled
            : Qt::NoItemFlags;
}

/////////////////////////////////////////////////////////////////////////
bool MessageModel::canFetchMore() const
{
    return m_canFetchMore and m_max_rows > rowCount();
}

/////////////////////////////////////////////////////////////////////////
QVariant MessageModel::data(const QModelIndex &idx, int role) const
{
    auto col     = idx.column();
    auto item    = m_getItem(idx);
    auto message = item->message;

    if (role == Qt::DisplayRole) {
        switch (col) {
            case 0:
                return message->text;

            case 1:
                return format_date(item->setTime);

            case 2:
                return format_date(item->resetTime);

            default:
                break;
        }
    }

    if (role == Qt::ToolTipRole)
        return Process::messageToolTip(
                m_process, item->message->path, item->message->index);

    if (role == Qt::UserRole) {
        auto path = QString::fromStdString(message->path);
        if (message->index >= 0)
            path += "//" + QString::number(message->index);
        return path;
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::fetchMore()
{
    if (not canFetchMore())
        return;

    if (m_message_items.empty()) {
        m_canFetchMore = false;
        m_messageManager->activeMessages(weak_from_this());
    }
    else if (not m_busy) {
        m_getNextMessage();
    }
}

/////////////////////////////////////////////////////////////////////////
size_t MessageModel::rowCount() const
{
    return m_message_items.size();
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::processMessage(const PdCom::Message &m)
{
    auto message    = m_findMessage(m);
    auto active_end = m_firstResetRow();

    auto set_it = std::lower_bound(
            m_message_items.begin(), active_end, message->setSeqNo);

    if (set_it != active_end and *set_it == message->setSeqNo) {
        auto reset_it = std::lower_bound(
                active_end, m_message_items.end(), message->setSeqNo);

        auto set_row   = std::distance(m_message_items.begin(), set_it);
        auto reset_row = std::distance(m_message_items.begin(), reset_it);

        m_parent->beginMoveRows(       //
                {}, set_row, set_row,  //
                {}, reset_row);
        set_it->resetTime = make_time(m);
        std::rotate(set_it, std::next(set_it), reset_it);
        m_parent->endMoveRows();

        emit m_parent->dataChanged(
                m_parent->createIndex(reset_row - 1, 0),  //
                m_parent->createIndex(reset_row - 1, 2),  //
                {Qt::DisplayRole});
    }

    if (not is_reset(m)) {
        m_parent->beginInsertRows({}, 0, 0);
        m_message_items.emplace(m_message_items.begin(), message, m);
        message->setSeqNo = m.seqNo;
        m_parent->endInsertRows();
    }

    setRowLimit(m_max_rows);
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::getMessageReply(const PdCom::Message &m)
{
    m_busy = false;

    // m_seqNo might have changed in the mean time, such as when
    // setRowLimit() is called
    if (m.seqNo != m_seqNo) {
        if (m_max_rows > rowCount())
            m_getNextMessage();
        return;
    }

    // An empty message path means no more messages
    m_canFetchMore = not m.path.empty();
    if (not m_canFetchMore)
        return;

    --m_seqNo;

    auto message         = m_findMessage(m);
    auto saved_resetTime = std::move(message->resetTime);
    message->resetTime   = make_time(m);

    if (is_reset(m))
        return m_getNextMessage();

    auto it = std::lower_bound(
            m_firstResetRow(), m_message_items.end(), m.seqNo);
    auto row = std::distance(m_message_items.begin(), it);

    m_parent->beginInsertRows({}, row, row);
    it            = m_message_items.emplace(it, message, m);
    it->resetTime = saved_resetTime;
    m_parent->endInsertRows();
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::activeMessagesReply(std::vector<PdCom::Message> messages)
{
    // At this point:
    //          m_canFetchMore == false

    assert(not m_canFetchMore);

    if (messages.empty())
        return;

    // Set up some pointers
    auto back_it = std::prev(messages.end(), 1);
    auto &back   = *back_it;

    // Seed m_seqNo from the last sequence number
    m_seqNo = back.seqNo - 1;

    if (is_reset(back)) {
        m_findMessage(back)->resetTime = make_time(back);

        // Special case of only on reset message
        if (messages.size() == 1)
            return m_getNextMessage();

        back_it = std::prev(back_it);
    }

    std::for_each(
            messages.begin(), back_it,
            std::bind(&MessageModel::processMessage, this, _1));

    m_canFetchMore = true;

    processMessage(*back_it);
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::m_disconnected()
{
    m_canFetchMore = false;

    auto row = std::distance(m_message_items.begin(), m_firstResetRow());
    if (row) {
        emit m_parent->dataChanged(
                m_parent->createIndex(0, 0),
                m_parent->createIndex(row - 1, 1), {Qt::DisplayRole});
    }
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::m_connected()
{
    m_parent->beginResetModel();
    m_message_items.clear();
    m_messages.clear();
    m_canFetchMore = true;
    m_busy         = false;
    m_parent->endResetModel();

    auto name = m_parent->objectName().toStdString();
    if (name.empty())
        name = m_process.lock()->parent->name();
}

/////////////////////////////////////////////////////////////////////////
const MessageModel::MessageItem *
MessageModel::m_getItem(const QModelIndex &idx) const
{
    return &*std::next(m_message_items.begin(), idx.row());
}

/////////////////////////////////////////////////////////////////////////
MessageModel::MessageItemList::iterator MessageModel::m_firstResetRow()
{
    return std::lower_bound(
            m_message_items.begin(), m_message_items.end(), true,
            [](auto &item, auto) { return item.resetTime.isNull(); });
}

/////////////////////////////////////////////////////////////////////////
MessageModel::Message *MessageModel::m_findMessage(const PdCom::Message &m)
{
    auto it = std::lower_bound(m_messages.begin(), m_messages.end(), m);

    if (it == m_messages.end() or not(*it == m))
        it = m_messages.emplace(it, m);

    if (it->original_text.empty() and not m.text.empty()) {
        it->original_text = m.text;
        it->text = Process::messageText(m_process, m.path, m.index, m.text);
    }

    return &*it;
}

/////////////////////////////////////////////////////////////////////////
void MessageModel::m_getNextMessage()
{
    m_busy = true;

    auto active_end = m_firstResetRow();

    auto it = std::lower_bound(m_message_items.begin(), active_end, m_seqNo);
    while (it != active_end and it->seqNo == m_seqNo) {
        --m_seqNo;
        std::advance(it, 1);
    }

    return m_messageManager->getMessage(weak_from_this(), m_seqNo);
}
