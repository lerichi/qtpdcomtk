/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageManager.h"
#include "MessageModel.h"
#include "Process.h"

using namespace qtpdcomtk::_private;

struct MessageManager::Private
{};

/////////////////////////////////////////////////////////////////////////////
MessageManager::MessageManager(const std::weak_ptr<Process> &p, Private) :
    m_process {p}
{
    Process::setMessageManager(m_process, this);
}

/////////////////////////////////////////////////////////////////////////////
MessageManager::~MessageManager()
{
    Process::setMessageManager(m_process, nullptr);
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::getMessage(
        const std::weak_ptr<MessageModel> &model,
        uint32_t seqNo)
{
    m_getMessageQ.push(model);
    MessageManagerBase::getMessage(seqNo);
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::activeMessages(const std::weak_ptr<MessageModel> &model)
{
    m_activeMessagesQ.push(model);
    MessageManagerBase::activeMessages();
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::add(MessageModel *m)
{
    m_messageModels.insert(m);
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::remove(MessageModel *m)
{
    m_messageModels.erase(m);
}

/////////////////////////////////////////////////////////////////////////////
std::shared_ptr<MessageManager>
MessageManager::create(const std::weak_ptr<Process> &p)
{
    return std::make_shared<MessageManager>(p, Private {});
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::processMessage(PdCom::Message m)
{
    for (auto model : m_messageModels)
        model->processMessage(m);
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::getMessageReply(PdCom::Message m)
{
    if (auto model = m_getMessageQ.front().lock())
        model->getMessageReply(m);
    m_getMessageQ.pop();
}

/////////////////////////////////////////////////////////////////////////////
void MessageManager::activeMessagesReply(std::vector<PdCom::Message> m)
{
    if (auto model = m_activeMessagesQ.front().lock())
        model->activeMessagesReply(std::move(m));
    m_activeMessagesQ.pop();
}
