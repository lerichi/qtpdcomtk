/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Variable.h"

#include "PdComTypeInfo.h"

#include <tuple>
#include <unordered_map>

using namespace qtpdcomtk::_private;

/////////////////////////////////////////////////////////////////////////////
Variable::Variable(PdCom::Variable var, const std::vector<size_t> &element) :
    PdCom::Variable {var}
{
    if (empty())
        return;

    auto size_info = getSizeInfo();
    m_nelem        = size_info.totalElements();
    m_count        = m_nelem;
    m_offset       = 0;
    for (size_t i = 0; i < element.size(); i++) {
        m_count /= size_info[i];
        m_offset += element[i] * m_count;
    }

#define _OP(T) \
    { \
        PdCom::TypeInfo::T, \
        { \
            &Variable::m_getVector<PdCom::TypeInfo::T>, \
                    &Variable::m_getScalar<PdCom::TypeInfo::T>, \
                    &Variable::m_setVector<PdCom::TypeInfo::T>, \
                    &Variable::m_setScalar<PdCom::TypeInfo::T>, \
        } \
    }
    static const auto _ops = std::unordered_map<
            PdCom::TypeInfo::DataType,
            std::tuple<GetVector_t, GetScalar_t, SetVector_t, SetScalar_t>> {
            _OP(boolean_T), _OP(char_T),  _OP(uint8_T),  _OP(int8_T),
            _OP(uint16_T),  _OP(int16_T), _OP(uint32_T), _OP(int32_T),
            _OP(uint64_T),  _OP(int64_T), _OP(double_T), _OP(single_T),
    };
    std::tie(m_getVectorPtr, m_getScalarPtr, m_setVectorPtr, m_setScalarPtr) =
            _ops.find(getTypeInfo().type)->second;

    if (m_count == 1)
        m_getVectorPtr = &Variable::m_readScalar;
}

/////////////////////////////////////////////////////////////////////////////
template <PdCom::TypeInfo::DataType T>  //
QVariant Variable::m_getScalar(const void *data, size_t idx)
{
    auto *src = reinterpret_cast<const pdcom_type<T> *>(data) + idx;
    return QVariant::fromValue<qt_type<T>>(*src);
}

/////////////////////////////////////////////////////////////////////////////
template <PdCom::TypeInfo::DataType T>  //
QVariant Variable::m_getVector(const void *data) const
{
    auto *src = reinterpret_cast<const pdcom_type<T> *>(data) + m_offset;
    QVariantList dst;
    dst.reserve(m_count);
    for (auto &v : dst)
        v.setValue<qt_type<T>>(*src++);
    return dst;
}

/////////////////////////////////////////////////////////////////////////////
template <PdCom::TypeInfo::DataType T>  //
bool Variable::m_setVector(const QVariantList &val, size_t idx) const
{
    using ctype = pdcom_type<T>;
    ctype dst[val.size()];
    std::transform(
            val.begin(), val.end(), dst,  //
            [](const QVariant &v) { return v.value<ctype>(); });
    PdCom::Variable::setValue(dst, T, m_count, m_offset + idx);

    return true;
}

/////////////////////////////////////////////////////////////////////////////
template <PdCom::TypeInfo::DataType T>  //
bool Variable::m_setScalar(const QVariant &_val, size_t idx) const
{
    auto val = _val.value<pdcom_type<T>>();

    PdCom::Variable::setValue(&val, T, 1, m_offset + idx);

    return true;
}
