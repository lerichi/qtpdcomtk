/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QVariant>
#include <functional>  // std::greater_equal
#include <pdcom5/Variable.h>
#include <vector>

namespace qtpdcomtk { namespace _private {

struct Variable : PdCom::Variable
{
    Variable() = default;

    Variable(PdCom::Variable var, const std::vector<size_t> &);

    size_t size() const { return m_nelem; }

    QVariant getScalar(const void *data, size_t idx = 0) const
    {
        return m_nelem and greater_equal(m_count, m_offset + idx + 1)
                ? std::invoke(m_getScalarPtr, data, idx + m_offset)
                : QVariant {};
    }

    QVariant getValue(const void *data) const
    {
        return m_nelem ? std::invoke(m_getVectorPtr, this, data)
                       : QVariant {};
    }

    bool setValue(const QVariant &val, size_t idx = 0) const
    {
        if (m_nelem == 0)
            return false;

        if (static_cast<QMetaType::Type>(val.type())
            == QMetaType::QVariantList) {
            auto src = val.toList();

            return greater_equal(m_count, src.size() + idx)
                    and std::invoke(m_setVectorPtr, this, src, idx);
        }
        else
            return greater_equal(m_count, m_offset + idx + 1)
                    and std::invoke(m_setScalarPtr, this, val, idx);
    }

  private:
    size_t m_offset {0};
    size_t m_count {1};
    size_t m_nelem {0};

    static constexpr auto greater_equal = std::greater_equal<size_t> {};

    QVariant m_readScalar(const void *data) const { return getScalar(data); }

    template <PdCom::TypeInfo::DataType T>
    static QVariant m_getScalar(const void *, size_t);
    using GetScalar_t = QVariant (*)(const void *data, size_t);
    GetScalar_t m_getScalarPtr;

    template <PdCom::TypeInfo::DataType T>
    QVariant m_getVector(const void *) const;
    using GetVector_t = QVariant (Variable::*)(const void *data) const;
    GetVector_t m_getVectorPtr;

    template <PdCom::TypeInfo::DataType>
    bool m_setVector(const QVariantList &, size_t) const;
    using SetVector_t =
            bool (Variable::*)(const QVariantList &val, size_t) const;
    SetVector_t m_setVectorPtr;

    template <PdCom::TypeInfo::DataType>
    bool m_setScalar(const QVariant &, size_t) const;
    using SetScalar_t = bool (Variable::*)(const QVariant &val, size_t) const;
    SetScalar_t m_setScalarPtr;
};

}}  // namespace qtpdcomtk::_private
