/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "VariableTreeModel.h"

#include "PdComTypeInfo.h"
#include "Process.h"

#include <QElapsedTimer>
#include <QIcon>
#include <QTimer>
#include <algorithm>   // std::lower_bound, std::max
#include <functional>  // std::invoke
#include <iterator>    // std::next
#include <list>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <tuple>  // std::tie
#include <unordered_set>
#include <vector>

using namespace qtpdcomtk::_private;

struct VariableTreeModel::Private
{};

namespace {

/////////////////////////////////////////////////////////////////////////
QVariant tool_tip(
        const qtpdcomtk::VariableTreeModel *model,
        const PdCom::Variable &var)
{ /*{{{*/
    if (var.empty())
        return {};

    QStringList tip;

    {
        auto name =
                model->tr(QT_TRANSLATE_NOOP(
                                  "qtpdcomtk::VariableTreeModel", "Path: %1"))
                        .arg(QString::fromStdString(var.getPath()));

        auto alias = var.getAlias();
        if (!alias.empty())
            name += '(' + QString::fromStdString(alias) + ')';

        tip << name;
    }

    {
        auto type = QString {var.getTypeInfo().ctype};

        auto sizeInfo = var.getSizeInfo();
        if (!sizeInfo.isScalar())
            for (auto i : sizeInfo)
                type += QString {"[%1]"}.arg(i);

        tip << model->tr(QT_TRANSLATE_NOOP(
                                 "qtpdcomtk::VariableTreeModel", "Type: %1"))
                        .arg(type);
    }

    if (auto sampleTime = var.getSampleTime().count())
        tip << model->tr(QT_TRANSLATE_NOOP(
                                 "qtpdcomtk::VariableTreeModel",
                                 "Task: %1 @ %2 s"))
                        .arg(var.getTaskId())
                        .arg(sampleTime);

    if (var.isWriteable())
        tip << model->tr(QT_TRANSLATE_NOOP(
                "qtpdcomtk::VariableTreeModel", "Writeable: 1"));

    return tip.join("\n");
} /*}}}*/

/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
struct Subscription : PdCom::Subscriber
{ /*{{{*/
    Subscription(
            VariableTreeModel::Node *n,
            const VariableTreeModel *m,
            const PdCom::Variable &var) :
        PdCom::Subscriber {
                var.isWriteable() ? PdCom::Transmission {PdCom::event_mode}
                                  : PdCom::Transmission {PdCom::poll_mode}},
        m_node {n},
        m_model {m}
    {
        m_subscription = PdCom::Subscription {*this, var};
        m_values.resize(var.getSizeInfo().totalElements());
        m_writeable = var.isWriteable();

        m_timer.callOnTimeout([this] { m_poll(); });
        m_timer.setSingleShot(true);

#define _OPS(PDCOM_TYPE) \
    { \
        PdCom::TypeInfo::PDCOM_TYPE, \
        { \
            &Subscription::m_read_func<PdCom::TypeInfo::PDCOM_TYPE>, \
                    &Subscription::m_write_func<PdCom::TypeInfo::PDCOM_TYPE> \
        } \
    }
        static const auto ops = std::unordered_map<
                PdCom::TypeInfo::DataType, std::tuple<ReadFunc, WriteFunc>> {
                _OPS(boolean_T), _OPS(char_T),    //
                _OPS(uint8_T),   _OPS(int8_T),    //
                _OPS(uint16_T),  _OPS(int16_T),   //
                _OPS(uint32_T),  _OPS(int32_T),   //
                _OPS(uint64_T),  _OPS(int64_T),   //
                _OPS(single_T),  _OPS(double_T),  //
        };

        std::tie(m_readValue, m_writeValue) =
                ops.find(var.getTypeInfo().type)->second;
    }

    QVariant value(size_t idx)
    {
        if (not m_busy) {
            m_busy = true;

            m_timer.start(std::max(0LL, 300 - m_elapsed_timer.elapsed()));
        }

        return idx < m_values.size() ? m_values[idx] : QVariant {};
    }

    bool setData(size_t offset, const QVariant &value) const
    {
        return std::invoke(m_writeValue, this, offset, value);
    }

    Qt::ItemFlags flags(size_t idx) const
    {
        auto var = m_subscription.getVariable();

        if (idx >= m_values.size() or var.empty())
            return Qt::NoItemFlags;

        return var.isWriteable()  //
                ? (Qt::ItemIsEnabled | Qt::ItemIsEditable)
                : Qt::ItemIsEnabled;
    }

    void disconnected()
    {
        m_busy = true;
        m_timer.stop();
    }

    size_t size() const { return m_values.size(); }

  private:
    VariableTreeModel::Node *const m_node;
    const VariableTreeModel *const m_model;

    PdCom::Subscription m_subscription;
    QElapsedTimer m_elapsed_timer;
    std::vector<QVariant> m_values;
    QTimer m_timer;
    bool m_busy {true};
    bool m_writeable;

    using ReadFunc = void (Subscription::*)();
    ReadFunc m_readValue;

    using WriteFunc = bool (Subscription::*)(size_t, const QVariant &) const;
    WriteFunc m_writeValue;

    void m_poll()
    {
        m_elapsed_timer.start();
        m_subscription.poll();
    }

    void stateChanged(const PdCom::Subscription &) final
    {
        m_busy = true;

        if (m_subscription.getState() == PdCom::Subscription::State::Active)
            m_poll();
    }

    void newValues(std::chrono::nanoseconds) final
    {
        m_busy = m_writeable;

        std::invoke(m_readValue, this);

        m_model->dataChanged(m_node, 1, size());
    }

    template <pdcom_datatype_t T>  //
    void m_read_func()
    {
        auto *data = reinterpret_cast<const pdcom_type<T> *>(
                m_subscription.getData());

        for (auto &item : m_values)
            item.setValue<qt_type<T>>(*data++);
    }

    template <pdcom_datatype_t T>  //
    bool m_write_func(size_t offset, const QVariant &value) const
    {
        using ctype = pdcom_type<T>;

        auto var = m_subscription.getVariable();
        if (var.empty() or not value.canConvert<ctype>())
            return false;

        auto data = value.value<ctype>();

        var.setValue(&data, T, 1, offset);
        return true;
    }
}; /*}}}*/

}  // namespace

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
struct VariableTreeModel::Node
{ /*{{{*/
    Node(Node *parent_, QString name_) : name {name_}, parent {parent_} {}

    QString const name;
    Node *const parent;

    bool cached;

    ////////////////////////////////////////////////////////////////////////
    QString path() const
    {
        return (parent ? parent->path() : QString {}) + name + '/';
    }

    ////////////////////////////////////////////////////////////////////////
    QVariant toolTip(const qtpdcomtk::VariableTreeModel *model_) const
    {
        return tool_tip(model_, m_variable);
    }

    ////////////////////////////////////////////////////////////////////////
    void disconnected(VariableTreeModel *model)
    {
        if (m_subscription)
            m_subscription->disconnected();

        for (auto &node : m_children) {
            if (not node.cached) {
                node.cached = true;
                model->dataChanged(&node, 0, 0);
            }

            node.disconnected(model);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    auto find(const QString &name_)
    {
        return std::lower_bound(
                m_children.begin(), m_children.end(), name_,
                [](auto &node, auto &name__) { return node.name < name__; });
    }

    ////////////////////////////////////////////////////////////////////////
    Node *findNextCacheNode()
    {
        size_t row_ = 0;

        for (auto *node = this; node;
             row_ = node->m_row + 1, node = node->parent) {
            for (auto child = std::next(node->m_children.begin(), row_);
                 child != node->m_children.end(); ++child)
                if (not child->m_children.empty())
                    return &*child;
        }

        return nullptr;
    }

    ////////////////////////////////////////////////////////////////////////
    void
    insert(QString name_,
           VariableTreeModel *model,
           bool is_dir,
           PdCom::Variable var = {})
    { /*{{{*/
        auto pos     = find(name_);
        auto cached_ = not is_dir;

        if (pos == m_children.end() or pos->name != name_) {
            // new node
            int row_ = std::distance(m_children.begin(), pos);
            model->m_tree_model->beginInsertRows(
                    modelIndex(model, 0), row_, row_);
            pos             = m_children.emplace(pos, this, name_);
            pos->cached     = cached_;
            pos->m_variable = var;
            m_renumber(pos, row_);
            model->m_tree_model->endInsertRows();
        }
        else {
            // modify node

            if (pos->m_subscription or not var.empty()
                or pos->cached != cached_) {
                pos->m_variable = var;
                pos->cached     = cached_;
                pos->m_subscription.reset();

                model->dataChanged(&*pos, 0, model->m_columns - 1);
            }

            if (cached_ and not pos->m_children.empty()) {
                model->m_tree_model->beginRemoveRows(
                        pos->modelIndex(model, 0),  //
                        0, pos->m_children.size() - 1);
                pos->m_children.clear();
                model->m_tree_model->endRemoveRows();
            }
        }
    } /*}}}*/

    ////////////////////////////////////////////////////////////////////////
    void erase(QString name_, VariableTreeModel *model)
    {
        auto pos = find(name_);
        int row_ = pos->m_row;

        model->m_tree_model->beginRemoveRows(
                modelIndex(model, 0), row_, row_);
        m_renumber(m_children.erase(pos), row_);
        model->m_tree_model->endRemoveRows();
    }

    ////////////////////////////////////////////////////////////////////////
    QVariant data(size_t idx, const VariableTreeModel *model)
    {
        if (m_subscription)
            return m_subscription->value(idx);

        if (not m_variable.empty())
            m_subscription =
                    std::make_unique<Subscription>(this, model, m_variable);

        return {};
    }

    ////////////////////////////////////////////////////////////////////////
    Qt::ItemFlags flags(size_t idx) const
    {
        return m_subscription  //
                ? m_subscription->flags(idx)
                : Qt::NoItemFlags;
    }

    ////////////////////////////////////////////////////////////////////////
    bool setData(size_t idx, const QVariant &value) const
    {
        return m_subscription and m_subscription->setData(idx, value);
    }

    ////////////////////////////////////////////////////////////////////////
    QModelIndex modelIndex(const VariableTreeModel *model, int col) const
    {
        return parent  //
                ? model->m_tree_model->createIndex(
                        m_row, col, const_cast<Node *>(this))
                : QModelIndex {};
    }

    ////////////////////////////////////////////////////////////////////////
    const Node *child(size_t idx) const
    {
        return idx < m_children.size()  //
                ? &*std::next(m_children.begin(), idx)
                : nullptr;
    }

    ////////////////////////////////////////////////////////////////////////
    size_t count() const { return m_children.size(); }

    ////////////////////////////////////////////////////////////////////////
    bool is_writeable() const
    {
        return not m_variable.empty() and m_variable.isWriteable();
    }

  private:
    std::list<Node> m_children;

    PdCom::Variable m_variable;
    size_t m_row;

    std::unique_ptr<Subscription> m_subscription;

    ////////////////////////////////////////////////////////////////////////
    template <typename Iterator>  //
    void m_renumber(Iterator pos, size_t row_)
    {
        for (; pos != m_children.end(); ++pos)
            pos->m_row = row_++;
    }
}; /*}}}*/

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
VariableTreeModel::VariableTreeModel(
        qtpdcomtk::VariableTreeModel *m,
        qtpdcomtk::Process *p,
        Private) :
    m_process {Process::weak_ptr(p)},
    m_tree_model {m},
    m_root {std::make_unique<Node>(nullptr, QString {})}
{
    m_signal_list                 //
            << QObject::connect(  //
                       p, &qtpdcomtk::Process::connected,
                       [this] { m_connected(); })
            << QObject::connect(  //
                       p, &qtpdcomtk::Process::disconnected,
                       [this] { m_disconnected(); });

    if (p->isConnected())
        m_connected();
    else
        m_disconnected();
}

/////////////////////////////////////////////////////////////////////////
VariableTreeModel::~VariableTreeModel()
{
    for (auto &s : m_signal_list)
        QObject::disconnect(s);
}

/////////////////////////////////////////////////////////////////////////
std::shared_ptr<VariableTreeModel> VariableTreeModel::create(
        qtpdcomtk::VariableTreeModel *model,
        qtpdcomtk::Process *m_process)
{
    return std::make_shared<VariableTreeModel>(model, m_process, Private {});
}

/////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::canFetchMore(const QModelIndex &idx) const
{
    return not m_resolve(idx)->cached and m_canHaveChildren(idx)
            and Process::is_connected(m_process);
}

/////////////////////////////////////////////////////////////////////////
int VariableTreeModel::columnCount(const QModelIndex &) const
{
    return m_columns;
}

/////////////////////////////////////////////////////////////////////////
QVariant VariableTreeModel::data(const QModelIndex &idx, int role) const
{
    auto col   = idx.column();
    auto *node = reinterpret_cast<Node *>(idx.internalPointer());

    // Reject invalid requests
    if (not node or not m_columnValid(col))
        return {};

    if (role == Qt::DisplayRole)
        return col > 0 ? node->data(col - 1, this) : node->name;

    if (role == Qt::EditRole and col > 0)
        return node->data(col - 1, this).toString();

    if (role == Qt::ToolTipRole and col == 0)
        return node->toolTip(m_tree_model);

    if (role == Qt::DecorationRole and col == 0 and node->is_writeable())
        return QIcon::fromTheme("preferences-system");

    return {};
}

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::fetchMore(const QModelIndex &idx)
{
    if (canFetchMore(idx))
        m_list(m_resolve(idx));
}

/////////////////////////////////////////////////////////////////////////
Qt::ItemFlags VariableTreeModel::flags(const QModelIndex &idx) const
{
    auto col = idx.column();

    if (col == 0)
        return Qt::ItemIsEnabled;

    if (m_columnValid(col))
        return m_resolve(idx)->flags(col - 1);

    return Qt::NoItemFlags;
}

/////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::hasChildren(const QModelIndex &idx) const
{
    return m_canHaveChildren(idx) and (rowCount(idx) or canFetchMore(idx));
}

/////////////////////////////////////////////////////////////////////////
QModelIndex
VariableTreeModel::index(int row, int col, const QModelIndex &idx) const
{
    auto *child = m_resolve(idx)->child(row);
    return child and m_canHaveChildren(idx) and m_columnValid(col)
            ? child->modelIndex(this, col)
            : QModelIndex {};
}

/////////////////////////////////////////////////////////////////////////
QModelIndex VariableTreeModel::parent(const QModelIndex &idx) const
{
    auto *parent = m_resolve(idx)->parent;
    return parent ? parent->modelIndex(this, 0) : QModelIndex {};
}

/////////////////////////////////////////////////////////////////////////
int VariableTreeModel::rowCount(const QModelIndex &idx) const
{
    return m_resolve(idx)->count();
}

/////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::setData(
        const QModelIndex &idx,
        const QVariant &value,
        int role)
{
    return role == Qt::EditRole
            and m_resolve(idx)->setData(idx.column() - 1, value);
}

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::dataChanged(Node *node, size_t begin, size_t end)
        const
{
    emit m_tree_model->dataChanged(
            node->modelIndex(this, begin), node->modelIndex(this, end),
            {Qt::DisplayRole});
}

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::m_insert(
        std::vector<PdCom::Variable> &variables,
        std::vector<std::string> &dirs)
{ /*{{{*/
    assert(not m_listQueue.empty());
    auto *node = m_listQueue.front();
    m_listQueue.pop();

    // Make a list of new directories, only the name part after the
    // last
    // '/'
    std::unordered_set<QString> dir_names;
    for (auto &path : dirs)
        dir_names.insert(QString::fromStdString(
                path.substr(path.find_last_of('/') + 1)));

    // Populate a deletion list of all current children that are
    // not already contained in the dir_names list.
    // This list will include new variables for the moment, but they
    // are removed in due coarse.
    std::unordered_set<QString> deletion_list;
    for (auto i = 0U; i < node->count(); ++i) {
        auto *child = node->child(i);
        if (dir_names.find(child->name) == dir_names.end())
            deletion_list.insert(child->name);
    }

    // Insert new variables.
    size_t columns {0};
    for (auto &v : variables) {
        auto name_  = QString::fromStdString(v.getName());
        auto dir_it = dir_names.find(name_);
        auto is_dir = dir_it != dir_names.end();

        deletion_list.erase(name_);  // Clean up deletion list

        // If the current variable also appears as a new directory,
        // remove it from that list so that the remainder is a list
        // of directories to be created
        if (is_dir)
            dir_names.erase(dir_it);

        // Update column count
        columns = std::max(columns, v.getSizeInfo().totalElements());

        node->insert(name_, this, is_dir, v);
    }

    // Make sure there are enough columns, +1 for name column
    if (not m_columnValid(++columns)) {
        m_tree_model->beginInsertColumns({}, m_columns, columns - 1);
        m_columns = columns;
        m_tree_model->endInsertColumns();
    }

    // Now we can remove children not required any more
    for (auto &name_ : deletion_list)
        node->erase(name_, this);

    // Go through the remainder of new directories and insert them
    for (auto &name_ : dir_names)
        node->insert(name_, this, true);

    // If refreshing tree, continue with next node
    if (node == m_cacheRefreshNode
        and (m_cacheRefreshNode = node->findNextCacheNode()))
        m_list(m_cacheRefreshNode);
} /*}}}*/

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::m_connected()
{
    if (m_columns > 1) {
        m_tree_model->beginRemoveColumns({}, 1, m_columns - 1);
        m_columns = 1;
        m_tree_model->endRemoveColumns();
    }

    m_cacheRefreshNode = m_root.get();
    m_list(m_cacheRefreshNode);
}

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::m_disconnected()
{
    // Setting the whole tree to cached will prevent node expansion
    m_root->cached = true;
    m_root->disconnected(this);

    // clear list queue
    m_listQueue = {};
}

/////////////////////////////////////////////////////////////////////////
void VariableTreeModel::m_list(Node *node)
{
    m_listQueue.push(node);
    node->cached = true;
    Process::list(m_process, weak_from_this(), node->path());
}

/////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::m_columnValid(size_t col) const
{
    return col < m_columns;
}

/////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::m_canHaveChildren(const QModelIndex &idx) const
{
    return not idx.isValid() or idx.column() == 0;
}

/////////////////////////////////////////////////////////////////////////
VariableTreeModel::Node *VariableTreeModel::m_resolve(const QModelIndex &idx)
{
    return idx.isValid()  //
            ? reinterpret_cast<Node *>(idx.internalPointer())
            : m_root.get();
}

/////////////////////////////////////////////////////////////////////////
const VariableTreeModel::Node *
VariableTreeModel::m_resolve(const QModelIndex &idx) const
{
    return idx.isValid()  //
            ? reinterpret_cast<Node *>(idx.internalPointer())
            : m_root.get();
}
