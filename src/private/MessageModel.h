/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "../../include/qtpdcomtk/MessageModel.h"
#include "Process.h"

#include <QDateTime>
#include <QMetaObject>
#include <list>
#include <memory>
#include <string>
#include <vector>

namespace qtpdcomtk {

class MessageModel;

namespace _private {

struct MessageModel : std::enable_shared_from_this<MessageModel>
{
    struct Private;

    MessageModel(qtpdcomtk::MessageModel *m, Private);
    ~MessageModel();

    void setProcess(qtpdcomtk::Process *);
    void setRowLimit(size_t);
    void retranslate();

    int rowLimit() const { return m_max_rows; }

    /////////////////////////////////////////////////////////////////////////
    static std::shared_ptr<MessageModel> create(qtpdcomtk::MessageModel *);

    /////////////////////////////////////////////////////////////////////////
    // QAbstractItemModel methods
    bool canFetchMore() const;
    Qt::ItemFlags flags(const QModelIndex &) const;
    QVariant data(const QModelIndex &, int role) const;
    void fetchMore();
    size_t rowCount() const;

    void processMessage(const PdCom::Message &);
    void getMessageReply(const PdCom::Message &);
    void activeMessagesReply(std::vector<PdCom::Message>);

  private:
    qtpdcomtk::MessageModel *const m_parent;

    QMetaObject::Connection m_process_connection, m_process_disconnection;
    std::shared_ptr<MessageManager> m_messageManager;
    std::weak_ptr<Process> m_process;
    using SeqNo_t = decltype(PdCom::Message::seqNo);
    SeqNo_t m_seqNo;
    size_t m_max_rows;
    bool m_canFetchMore;
    bool m_busy;

    struct Message
    {
        Message(const PdCom::Message &);

        std::string const path;
        int const index;
        std::string original_text;

        QString text;

        QDateTime resetTime;  // used for getMessage()
        SeqNo_t setSeqNo;     // used for processMessage()

        bool operator<(const PdCom::Message &m) const;
        bool operator==(const PdCom::Message &m) const;
    };
    std::list<Message> m_messages;

    struct MessageItem
    {
        MessageItem(Message *, const PdCom::Message &);

        Message *message;
        SeqNo_t seqNo;
        QDateTime setTime;
        QDateTime resetTime;

        bool operator<(SeqNo_t) const;
        bool operator==(SeqNo_t) const;
    };
    using MessageItemList = std::vector<MessageItem>;
    MessageItemList m_message_items;

    void m_connected();
    void m_disconnected();
    void m_getNextMessage();
    MessageItemList::iterator m_firstResetRow();
    Message *m_findMessage(const PdCom::Message &);
    void m_updateMessageText(const Message &) const;
    const MessageItem *m_getItem(const QModelIndex &) const;
};

}  // namespace _private
}  // namespace qtpdcomtk
