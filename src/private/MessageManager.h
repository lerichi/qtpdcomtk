/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <memory>
#include <pdcom5/MessageManagerBase.h>
#include <queue>
#include <unordered_set>

namespace qtpdcomtk { namespace _private {

struct MessageModel;
struct Process;

class MessageManager :
    public std::enable_shared_from_this<MessageManager>,
    private PdCom::MessageManagerBase
{
    friend Process;

    struct Private;

  public:
    MessageManager(const std::weak_ptr<Process> &, Private);
    ~MessageManager();

    void add(MessageModel *);
    void remove(MessageModel *);

    void getMessage(const std::weak_ptr<MessageModel> &, uint32_t seqNo);
    void activeMessages(const std::weak_ptr<MessageModel> &);

  private:
    const std::weak_ptr<Process> m_process;

    using MessageModelQueue = std::queue<std::weak_ptr<MessageModel>>;
    MessageModelQueue m_getMessageQ;
    MessageModelQueue m_activeMessagesQ;
    std::unordered_set<MessageModel *> m_messageModels;

    static std::shared_ptr<MessageManager>
    create(const std::weak_ptr<Process> &);
    void processMessage(PdCom::Message) final;
    void getMessageReply(PdCom::Message) final;
    void activeMessagesReply(std::vector<PdCom::Message>) final;
};

}}  // namespace qtpdcomtk::_private
