/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "../../include/qtpdcomtk/Process.h"
#include "../../include/qtpdcomtk/Subscriber.h"
#include "Variable.h"

#include <pdcom5/Subscription.h>

namespace qtpdcomtk { namespace _private {

struct ProcessSubscription : PdCom::Subscription
{
    ProcessSubscription(
            double ts,
            qtpdcomtk::Process *p,
            std::string _path,
            std::vector<size_t> sel) :
        process {p},
        sampleTime {ts},
        path {std::move(_path)},
        selector {std::move(sel)}
    {}
    ~ProcessSubscription() { unsubscribe(); }

    qtpdcomtk::Process *const process;
    double const sampleTime;
    std::string const path;
    std::vector<size_t> const selector;

    void set_subscriber(std::shared_ptr<PdCom::Subscriber> pdcom)
    {
        unsubscribe();
        m_pdcom = std::move(pdcom);
    }

    void subscribe()
    {
        static_cast<PdCom::Subscription &>(*this) =
                PdCom::Subscription {*m_pdcom, *process, path};
    }

    void unsubscribe() { static_cast<PdCom::Subscription &>(*this) = {}; }

    bool isActive() const
    {
        return getState() == PdCom::Subscription::State::Active;
    }

    virtual void stateChanged()
    {
        if (isActive())
            m_variable = Variable {getVariable(), selector};

        m_subscriber->stateChanged();
    }
    QVariant getValue() { return m_variable.getValue(getData()); }
    bool setValue(const QVariant &val) { return m_variable.setValue(val); }

    qtpdcomtk::Subscriber *subscriber() const { return m_subscriber; }
    void setSubscriber(qtpdcomtk::Subscriber *s) { m_subscriber = s; }

  private:
    std::shared_ptr<PdCom::Subscriber> m_pdcom;
    qtpdcomtk::Subscriber *m_subscriber {nullptr};

    Variable m_variable;
};

}}  // namespace qtpdcomtk::_private
