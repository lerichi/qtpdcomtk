/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include "../../include/qtpdcomtk/VariableTreeModel.h"

#include <QList>
#include <memory>
#include <queue>
#include <string>
#include <vector>

namespace qtpdcomtk {

class VariableTreeModel;

namespace _private {

struct Process;

struct VariableTreeModel : std::enable_shared_from_this<VariableTreeModel>
{
    friend Process;

    struct Private;
    struct Node;

    VariableTreeModel(
            qtpdcomtk::VariableTreeModel *m,
            qtpdcomtk::Process *p,
            Private);
    ~VariableTreeModel();

    /////////////////////////////////////////////////////////////////////////
    static std::shared_ptr<VariableTreeModel>
    create(qtpdcomtk::VariableTreeModel *, qtpdcomtk::Process *);
    void dataChanged(Node *node, size_t begin, size_t end) const;

    /////////////////////////////////////////////////////////////////////////
    // QAbstractItemModel methods
    bool canFetchMore(const QModelIndex &) const;
    int columnCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &, int role) const;
    void fetchMore(const QModelIndex &);
    Qt::ItemFlags flags(const QModelIndex &) const;
    bool hasChildren(const QModelIndex &) const;
    QModelIndex index(int row, int col, const QModelIndex &) const;
    QModelIndex parent(const QModelIndex &) const;
    int rowCount(const QModelIndex &) const;
    bool setData(const QModelIndex &, const QVariant &, int role);

  private:
    std::weak_ptr<Process> const m_process;
    qtpdcomtk::VariableTreeModel *const m_tree_model;

    std::unique_ptr<Node> m_root;

    size_t m_columns {1};
    Node *m_cacheRefreshNode;
    std::queue<Node *> m_listQueue;
    QList<QMetaObject::Connection> m_signal_list;

    void m_connected();
    void m_disconnected();
    void m_list(Node *node);
    void m_setDataColumns(size_t);
    Node *m_resolve(const QModelIndex &);
    bool m_columnValid(size_t col) const;
    bool m_canHaveChildren(const QModelIndex &) const;
    const Node *m_resolve(const QModelIndex &) const;
    void m_insert(std::vector<PdCom::Variable> &, std::vector<std::string> &);
};

}  // namespace _private
}  // namespace qtpdcomtk
