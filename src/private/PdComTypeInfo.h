/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QtGlobal>
#include <pdcom5/SizeTypeInfo.h>

// Make sure that this module is touched should PdCom::TypeInfo::DataType
// ever change
static_assert(PdCom::TypeInfo::DataTypeEnd == 12);

#if __cplusplus < 201703L
namespace std {
template <typename T> static constexpr bool is_signed_v =
        std::is_signed<T>::value;
}
#endif

namespace qtpdcomtk { namespace _private {

using pdcom_datatype_t = PdCom::TypeInfo::DataType;

// clang-format off

// Some templating magic to start off with.
//
// The aim is to map a pdcom data type to the best QVariant data type
//
// The general logic is:
//     * get the underlying C type from the variable
//     * derive a Qt type from this C type.
//
// The outcome is:
//      single_T, double_T -> double
//      boolean_T          -> bool
//      (u)int64_T         -> sizeof(int) == 8; then (u)int; else q(u)int64
//      uint*_T            -> unsigned int
//      int*_T             -> int

template <pdcom_datatype_t> struct pdcom_to_ctype;

#define PDCOM_TO_CTYPE(T1, T2) \
    template <> struct pdcom_to_ctype<PdCom::TypeInfo::T1> { using type = T2; }
PDCOM_TO_CTYPE(boolean_T,     bool);
PDCOM_TO_CTYPE(   char_T,     char);
PDCOM_TO_CTYPE(  uint8_T,  uint8_t);
PDCOM_TO_CTYPE(   int8_T,   int8_t);
PDCOM_TO_CTYPE( uint16_T, uint16_t);
PDCOM_TO_CTYPE(  int16_T,  int16_t);
PDCOM_TO_CTYPE( uint32_T, uint32_t);
PDCOM_TO_CTYPE(  int32_T,  int32_t);
PDCOM_TO_CTYPE( uint64_T, uint64_t);
PDCOM_TO_CTYPE(  int64_T,  int64_t);
PDCOM_TO_CTYPE( double_T,   double);
PDCOM_TO_CTYPE( single_T,    float);

template <typename T> using q_xinttype =
        typename std::conditional<std::is_signed_v<T>, int, uint>::type;

template <typename T> using q_xint64type =
        typename std::conditional<std::is_signed_v<T>, qint64, quint64>::type;

// General Qt type selector for integers
template <typename T> struct ctype_to_qtype
{
    using type = typename std::conditional<
            (sizeof(T) > sizeof(int)),
            q_xint64type<T>,
            q_xinttype<T> >::type;
};

// Type specialization for specific data types
template <> struct ctype_to_qtype<  bool> { using type =  bool; };
template <> struct ctype_to_qtype<double> { using type = qreal; };
template <> struct ctype_to_qtype< float> { using type = qreal; };
// clang-format on

/** Selects the underlying C type corresponding to a pdcom data type */
template <pdcom_datatype_t T> using pdcom_type =
        typename pdcom_to_ctype<T>::type;

/** Selects the best Qt data type representing a pdcom data type */
template <pdcom_datatype_t T> using qt_type =
        typename ctype_to_qtype<pdcom_type<T>>::type;

}}  // namespace qtpdcomtk::_private
