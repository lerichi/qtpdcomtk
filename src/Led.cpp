/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/Led.h"

#include <QPainter>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
Led::Led(QWidget *parent, bool _invert, int _dark) :
    Q_Widget {parent}, invert {_invert}, dark {_dark}
{}

/////////////////////////////////////////////////////////////////////////////
void Led::paint(QPainter *painter, const QRect &rect, const QPalette &palette)
{
    int width  = rect.width();
    int height = rect.height();
    int side   = qMin(width, height) - 2;

    painter->fillRect(rect, Qt::transparent);

    QColor color;
    if (m_state) {
        color = onColor.isValid() ? onColor
                                  : palette.color(QPalette::WindowText);
    }
    else {
        color = offColor.isValid()
                ? offColor
                : palette.color(QPalette::WindowText).darker(dark);
    }

    painter->setPen(Qt::black);
    painter->setBrush(color);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawEllipse(
            rect.x() + (width - side) / 2, rect.y() + (height - side) / 2,
            side, side);
}

/////////////////////////////////////////////////////////////////////////////
void Led::valueChanged(
        const std::chrono::nanoseconds &,
        const QVariant &value)
{
    m_state = value.toBool() ^ invert;
}
