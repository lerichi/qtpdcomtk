/****************************************************************************
 *
 * Copyright (C) 2024 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "private/VariableTreeModel.h"

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
VariableTreeModel::VariableTreeModel(Process *p, QObject *parent) :
    QAbstractItemModel {parent},
    p_impl {_private::VariableTreeModel::create(this, p)}
{}

/////////////////////////////////////////////////////////////////////////////
VariableTreeModel::~VariableTreeModel()
{}

/////////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::canFetchMore(const QModelIndex &idx) const
{
    return p_impl->canFetchMore(idx);
}

/////////////////////////////////////////////////////////////////////////////
int VariableTreeModel::columnCount(const QModelIndex &idx) const
{
    return p_impl->columnCount(idx);
}

/////////////////////////////////////////////////////////////////////////////
QVariant VariableTreeModel::data(const QModelIndex &idx, int role) const
{
    return p_impl->data(idx, role);
}

/////////////////////////////////////////////////////////////////////////////
void VariableTreeModel::fetchMore(const QModelIndex &idx)
{
    return p_impl->fetchMore(idx);
}

/////////////////////////////////////////////////////////////////////////////
Qt::ItemFlags VariableTreeModel::flags(const QModelIndex &idx) const
{
    return p_impl->flags(idx);
}

/////////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::hasChildren(const QModelIndex &idx) const
{
    return p_impl->hasChildren(idx);
}

/////////////////////////////////////////////////////////////////////////////
QVariant VariableTreeModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    if (role == Qt::DisplayRole and orientation == Qt::Horizontal) {
        if (section == 0)
            return tr("Name");
        return section;
    }

    return {};
}

/////////////////////////////////////////////////////////////////////////////
QModelIndex
VariableTreeModel::index(int row, int col, const QModelIndex &idx) const
{
    return p_impl->index(row, col, idx);
}

/////////////////////////////////////////////////////////////////////////////
QModelIndex VariableTreeModel::parent(const QModelIndex &idx) const
{
    return p_impl->parent(idx);
}

/////////////////////////////////////////////////////////////////////////////
int VariableTreeModel::rowCount(const QModelIndex &idx) const
{
    return p_impl->rowCount(idx);
}

/////////////////////////////////////////////////////////////////////////////
bool VariableTreeModel::setData(const QModelIndex &idx, const QVariant &value, int role)
{
    return p_impl->setData(idx, value, role);
}
