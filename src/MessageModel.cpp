/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "private/MessageModel.h"

#include <QAbstractItemView>
#include <QCoreApplication>
#include <QHeaderView>
#include <functional>  // std::bind

using namespace qtpdcomtk;
using namespace std::placeholders;

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
MessageModel::MessageModel(QObject *parent) :
    QAbstractTableModel {parent},
    p_impl {_private::MessageModel::create(this)}
{
    if (auto *view = qobject_cast<QAbstractItemView *>(parent))
        view->setModel(this);
}

///////////////////////////////////////////////////////////////////////////
MessageModel::~MessageModel()
{}

///////////////////////////////////////////////////////////////////////////
void MessageModel::setProcess(qtpdcomtk::Process *p)
{
    p_impl->setProcess(p);
}

///////////////////////////////////////////////////////////////////////////
bool MessageModel::canFetchMore(const QModelIndex &idx) const
{
    return not idx.isValid() and p_impl->canFetchMore();
}

///////////////////////////////////////////////////////////////////////////
void MessageModel::fetchMore(const QModelIndex &idx)
{
    if (canFetchMore(idx))
        p_impl->fetchMore();
}

///////////////////////////////////////////////////////////////////////////
Qt::ItemFlags MessageModel::flags(const QModelIndex &idx) const
{
    return idx.isValid() ? p_impl->flags(idx) : Qt::NoItemFlags;
}

///////////////////////////////////////////////////////////////////////////
QVariant MessageModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    if (orientation == Qt::Horizontal and role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                return tr("Message");
            case 1:
                return tr("Time");
            case 2:
                return tr("Reset");
        }
    }

    return {};
}

///////////////////////////////////////////////////////////////////////////
int MessageModel::rowCount(const QModelIndex &idx) const
{
    return idx.isValid() ? 0 : p_impl->rowCount();
}

///////////////////////////////////////////////////////////////////////////
int MessageModel::columnCount(const QModelIndex &) const
{
    return 3;
}

///////////////////////////////////////////////////////////////////////////
void MessageModel::setRowLimit(int count)
{
    if (count >= 0)
        p_impl->setRowLimit(count);
}

///////////////////////////////////////////////////////////////////////////
int MessageModel::rowLimit() const
{
    return p_impl->rowLimit();
}

///////////////////////////////////////////////////////////////////////////
QVariant MessageModel::data(const QModelIndex &idx, int role) const
{
    return idx.isValid() ? p_impl->data(idx, role) : QVariant {};
}

///////////////////////////////////////////////////////////////////////////
void MessageModel::retranslate()
{
    p_impl->retranslate();
}
