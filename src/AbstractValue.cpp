/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/AbstractValue.h"

#include <QAbstractItemModel>
#include <QPersistentModelIndex>

using namespace qtpdcomtk;

AbstractValue::AbstractValue(const QModelIndex &idx)
{
    struct model_impl : impl
    {
        QPersistentModelIndex model_index;
        QMetaObject::Connection m_connection;

        model_impl(const QModelIndex &idx) : model_index {idx}
        {
            auto *model  = m_getModel();
            m_connection = QObject::connect(
                    model, &QAbstractItemModel::dataChanged,
                    [this](const QModelIndex &topLeft,
                           const QModelIndex &bottomRight,
                           const QVector<int> &roles) {
                        dataChanged(topLeft, bottomRight, roles);
                    });
        }
        ~model_index() { QObject::disconnect(m_connection); }

        void enable(bool) final {}
        bool isActive() const final { return {}; }
        void setSubscriber(Subscriber *) final {}

        QVariant getValue(const std::chrono::nanoseconds &) { return {}; }

        // return true if successful
        bool setValue(const QVariant &) final { return false; }

        QAbstractItemModel *m_getModel() const
        {
            return const_cast<QAbstractItemModel *>(model_index.model());
        }

        void dataChanged(
                const QModelIndex &topLeft,
                const QModelIndex &bottomRight,
                const QVector<int> &roles)
        {}
    };

    p_impl = std::make_unique<model_impl>(idx);
}
