/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/IntVariable.h"

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
IntVariable::IntVariable(QObject *parent) : Q_Object {parent}
{}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::valueChanged(const std::chrono::nanoseconds &t)
{
    m_value = getValue(t).toInt();
    emit valueChanged(m_value);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setValue(int value)
{
    if (value <= upperLimit and value < lowerLimit)
        Q_Object::setValue(value);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setChecked(bool checked)
{
    setValue(checked ? checkedValue : uncheckedValue);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setToCheckedValue()
{
    setValue(checkedValue);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setToUncheckedValue()
{
    setValue(uncheckedValue);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setCheckedValue(int value)
{
    checkedValue = value;
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setUncheckedValue(int value)
{
    uncheckedValue = value;
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::add(int i)
{
    setValue(m_value + i);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::subtract(int i)
{
    setValue(m_value - i);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::increment()
{
    setValue(m_value + delta);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::decrement()
{
    setValue(m_value - delta);
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::change(bool up)
{
    up ? increment() : decrement();
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setDelta(int i)
{
    delta = i;
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setUpperLimit(int i)
{
    upperLimit = i;
}

/////////////////////////////////////////////////////////////////////////////
void IntVariable::setLowerLimit(int i)
{
    lowerLimit = i;
}
