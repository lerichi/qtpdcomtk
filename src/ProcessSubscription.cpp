/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/ProcessSubscription.h"

#include "../include/qtpdcomtk/Process.h"
#include "../include/qtpdcomtk/Subscriber.h"
#include "../include/qtpdcomtk/detail/overloaded.h"
#include "private/Process.h"
#include "private/ProcessSubscription.h"

#include <QElapsedTimer>
#include <QObject>
#include <QTimer>
#include <chrono>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct ProcessSubscription::impl :
    AbstractValue::impl,
    _private::ProcessSubscription
{
    impl(double _sampleTime,
         qtpdcomtk::Process *_process,
         QString _path,
         QVector<uint> sel) :
        _private::ProcessSubscription {
                _sampleTime,
                _process,
                _path.toStdString(),
                {sel.begin(), sel.end()}}
    {}

    // Reimplemented from AbstractValue::impl
    bool isActive() const final
    {
        return _private::ProcessSubscription::isActive();
    }
    QVariant getValue(const std::chrono::nanoseconds &) final
    {
        return _private::ProcessSubscription::getValue();
    }
    bool setValue(const QVariant &v) final
    {
        return _private::ProcessSubscription::setValue(v);
    }

    void setSubscriber(qtpdcomtk::Subscriber *s) final
    {
        _private::ProcessSubscription::setSubscriber(s);
    }

    void enable(bool state) override
    {
        if (state)
            subscribe();
        else
            unsubscribe();
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct ProcessSubscription::event_impl : impl
{
    struct SingleSubscriber : PdCom::Subscriber
    {
        SingleSubscriber(event_impl *_impl, const PdCom::Transmission &td) :
            PdCom::Subscriber {td}, impl {_impl}
        {}

        event_impl *const impl;

        void stateChanged(PdCom::Subscription const &) override
        {
            impl->stateChanged();
        }
        void newValues(std::chrono::nanoseconds ts) override
        {
            impl->valueChanged(ts);
        }
    };

    event_impl(
            double _sampleTime,
            const PdCom::Transmission &td,
            qtpdcomtk::Process *_process,
            QString _path,
            QVector<uint> sel) :
        impl {_sampleTime, _process, _path, sel}
    {
        set_subscriber(std::make_shared<SingleSubscriber>(this, td));
        subscribe();
    }

    virtual void valueChanged(const std::chrono::nanoseconds &ts)
    {
        subscriber()->valueChanged(ts);
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct ProcessSubscription::poll_impl : event_impl
{
    poll_impl(
            double ts,
            qtpdcomtk::Process *p,
            QString _path,
            QVector<uint> sel) :
        event_impl {ts, PdCom::poll_mode, p, _path, sel}
    {
        m_connection = m_delayTimer.callOnTimeout([this] { m_poll(); });
        m_delayTimer.setSingleShot(true);
    }
    ~poll_impl() { QObject::disconnect(m_connection); }

    void stateChanged() final
    {
        if (isActive())
            m_poll();

        ProcessSubscription::stateChanged();
    }

    void valueChanged(const std::chrono::nanoseconds &ts) final
    {
        using namespace std::chrono_literals;

        event_impl::valueChanged(ts);

        // auto diff_ms = std::chrono::floor<std::chrono::milliseconds>(
        //        std::chrono::duration<double> {sampleTime}
        //        - std::chrono::milliseconds {m_elapsedTimer.elapsed()});

        auto diff = static_cast<int>(
                1000.0 * sampleTime - m_elapsedTimer.elapsed());
        if (diff > 0)
            m_delayTimer.start(diff);
        else
            m_poll();
    }

    void enable(bool state) override
    {
        if (not state)
            m_delayTimer.stop();

        event_impl::enable(state);
    }

  private:
    QElapsedTimer m_elapsedTimer;
    QTimer m_delayTimer;
    QMetaObject::Connection m_connection;

    void m_poll()
    {
        m_elapsedTimer.start();
        PdCom::Subscription::poll();
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct ProcessSubscription::stream_impl : impl
{
    stream_impl(
            double ts,
            Process *_process,
            QString _path,
            QVector<uint> sel) :
        impl {ts, _process, _path, sel}
    {
        set_subscriber(_private::Process::getSubscriber(_process, ts));
        subscribe();
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
ProcessSubscription::ProcessSubscription(
        const Transmission::value_type &transmission,
        Process *process,
        QString path,
        QVector<uint> selector) :
    AbstractValue {std::visit(
            detail::overloaded {
                    [=](const event_t &) -> std::unique_ptr<impl> {
                        return std::make_unique<event_impl>(
                                0.0, PdCom::event_mode, process, path,
                                selector);
                    },
                    [=](const poll_t &_tx) -> std::unique_ptr<impl> {
                        return std::make_unique<poll_impl>(
                                _tx.sampleTime, process, path, selector);
                    },
                    [=](const stream_t &_tx) -> std::unique_ptr<impl> {
                        return std::make_unique<stream_impl>(
                                _tx.sampleTime, process, path, selector);
                    },
            },
            transmission)}
{}
