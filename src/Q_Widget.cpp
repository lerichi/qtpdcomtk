/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/Q_Widget.h"

#include <QEvent>
#include <QPainter>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
Q_Widget::Q_Widget(QWidget *parent) : Q_Object {parent}
{
    parent->installEventFilter(this);
}

/////////////////////////////////////////////////////////////////////////////
QWidget *Q_Widget::parent()
{
    return qobject_cast<QWidget *>(Q_Object::parent());
}

/////////////////////////////////////////////////////////////////////////////
void Q_Widget::valueChanged(const std::chrono::nanoseconds &t)
{
    valueChanged(t, getValue(t));
    parent()->update();
}

/////////////////////////////////////////////////////////////////////////////
bool Q_Widget::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() != QEvent::Paint)
        return QObject::eventFilter(object, event);

    m_paint();
    return true;
}

/////////////////////////////////////////////////////////////////////////////
void Q_Widget::m_paint()
{
    QWidget *widget = parent();
    QPainter painter {widget};

    paint(&painter, widget->rect(), widget->palette());
}
