/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/ModelIndexValue.h"

#include "../include/qtpdcomtk/Subscriber.h"

#include <QAbstractItemModel>
#include <QPersistentModelIndex>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct ModelIndexValue::impl : AbstractValue::impl
{
    QPersistentModelIndex modelIndex;
    QMetaObject::Connection connection;
    Subscriber *subscriber {nullptr};

    impl(const QModelIndex &idx) : modelIndex {idx}
    {
        connection = QObject::connect(
                idx.model(), &QAbstractItemModel::dataChanged,  //
                [this](const QModelIndex &topLeft,
                       const QModelIndex &bottomRight,
                       const QVector<int> &roles) {
                    dataChanged(topLeft, bottomRight, roles);
                });
    }
    ~impl() { QObject::disconnect(connection); }

    void enable(bool) final {}
    bool isActive() const final
    {
        return modelIndex.flags() & Qt::ItemIsEnabled;
    }
    void setSubscriber(Subscriber *s) final { subscriber = s; }

    QVariant getValue(const std::chrono::nanoseconds &)
    {
        return modelIndex.data(Qt::DisplayRole);
    }

    // return true if successful
    bool setValue(const QVariant &val) final
    {
        return m_model()->setData(modelIndex, val, Qt::DisplayRole);
    }

    QAbstractItemModel *m_model() const
    {
        return const_cast<QAbstractItemModel *>(modelIndex.model());
    }

    void dataChanged(
            const QModelIndex &topLeft,
            const QModelIndex &bottomRight,
            const QVector<int> &roles)
    {
        if (modelIndex < topLeft or bottomRight < modelIndex)
            return;

        if (roles.contains(Qt::DisplayRole))
            subscriber->valueChanged({});
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
ModelIndexValue::ModelIndexValue(const QModelIndex &idx) :
    AbstractValue {std::make_unique<impl>(idx)}
{}

ModelIndexValue::operator QModelIndex() const
{
    return get_impl<impl>()->modelIndex;
}
