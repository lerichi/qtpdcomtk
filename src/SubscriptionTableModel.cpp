/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/SubscriptionTableModel.h"

#include "../include/qtpdcomtk/Process.h"
#include "../include/qtpdcomtk/detail/overloaded.h"
#include "private/Variable.h"

#include <QDebug>
#include <QElapsedTimer>
#include <QEvent>
#include <QTimer>
#include <functional>  // std::greater_equal
#include <list>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <vector>

using namespace qtpdcomtk;

namespace {
struct Subscription : PdCom::Subscription
{
    Subscription(
            const std::shared_ptr<PdCom::Subscriber> &_subscriber,
            QString _path) :
        subscriber {_subscriber}, path {_path}
    {}
    ~Subscription() { unsubscribe(); }

    std::shared_ptr<PdCom::Subscriber> const subscriber;
    QString const path;

    void subscribe(Process *p)
    {
        static_cast<PdCom::Subscription &>(*this) =
                PdCom::Subscription {*subscriber, *p, path.toStdString()};
    }

    void unsubscribe() { static_cast<PdCom::Subscription &>(*this) = {}; }

    operator bool() const
    {
        return getState() == PdCom::Subscription::State::Active;
    }

    uint size() const { return m_variable.size(); }

    bool operator<(const QString &other_path) const
    {
        return path < other_path;
    }

    void stateChanged(SubscriptionTableModel *model)
    {
        m_variable = _private::Variable {getVariable(), {}};
        if (getState() == PdCom::Subscription::State::Invalid)
            emit model->unknownVariable(QString::fromStdString(getPath()));
    }

    QVariant getValue(uint col)
    {
        return m_variable.getScalar(getData(), col);
    }

  private:
    _private::Variable m_variable;
};
}  // namespace

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct SubscriptionTableModel::impl
{
    impl(SubscriptionTableModel *m) : model {m} {}
    virtual ~impl() {}

    SubscriptionTableModel *const model;

    Process *process() { return static_cast<Process *>(model->parent()); }

    void subscribe()
    {
        m_active = true;

        using std::placeholders::_1;
        std::for_each(
                m_subscription.begin(), m_subscription.end(),
                std::bind(&Subscription::subscribe, _1, process()));
    }
    void unsubscribe()
    {
        m_active = false;

        using std::placeholders::_1;
        std::for_each(
                m_subscription.begin(), m_subscription.end(),
                std::bind(&Subscription::unsubscribe, _1));
    }

    virtual Subscription *insert(QString, uint) = 0;
    uint columnCount() const { return m_columns; }
    uint rowCount() const { return m_subscription.size(); }
    QModelIndex index(uint row, uint col) const
    {
        return row < rowCount() and col < columnCount()
                ? model->createIndex(row, col)
                : QModelIndex {};
    }

    Subscription *
    insert(const std::shared_ptr<PdCom::Subscriber> &subscriber,
           QString path,
           uint col)
    {
        auto it = std::lower_bound(
                m_subscription.begin(), m_subscription.end(), path);

        if (it == m_subscription.end() or it->path != path) {
            auto row = std::distance(m_subscription.begin(), it);

            model->beginInsertRows({}, row, row);
            it = m_subscription.emplace(it, subscriber, path);
            model->endInsertRows();

            if (m_active)
                it->subscribe(process());
        }

        if (col >= m_columns) {
            model->beginInsertColumns({}, m_columns, col);
            m_columns = col + 1;
            model->endInsertColumns();
        }

        return &*it;
    }

    uint row(const Subscription *s) const
    {
        return std::distance(
                m_subscription.begin(),
                std::lower_bound(
                        m_subscription.begin(), m_subscription.end(),
                        s->path));
    }

    virtual void stateChanged(const PdCom::Subscription &s)
    {
        auto *subscription = static_cast<Subscription *>(
                const_cast<PdCom::Subscription *>(&s));

        subscription->stateChanged(model);

        auto row_ = row(subscription);
        auto col_ = *subscription ? subscription->size() : columnCount();

        emit model->dataChanged(
                index(row_, 0), index(row_, col_ - 1), {Qt::DisplayRole});
    }

    Qt::ItemFlags flags(const QModelIndex &idx)
    {
        auto *subscription = m_getItem(idx);

        if (not subscription)
            return Qt::NoItemFlags;

        if (std::greater_equal<uint> {}(idx.column(), subscription->size()))
            return Qt::NoItemFlags;

        return subscription->getVariable().isWriteable()
                ? Qt::ItemIsEnabled | Qt::ItemIsEditable
                : Qt::ItemIsEnabled;
    }

    QVariant data(const QModelIndex &idx, int role)
    {
        auto *subscription = m_getItem(idx);

        if (not subscription or not *subscription)
            return {};

        if (role == Qt::DisplayRole)
            return subscription->getValue(idx.column());

        return {};
    }

  private:
    uint m_columns {1};
    bool m_active {false};

    std::list<Subscription> m_subscription;

    Subscription *m_getItem(const QModelIndex &idx)
    {
        if (not idx.isValid())
            return nullptr;

        return &*std::next(m_subscription.begin(), idx.row());
    }
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct SubscriptionTableModel::event_impl : impl
{
    event_impl(SubscriptionTableModel *m) : impl {m} {}
    Subscription *insert(QString path, uint col) override
    {
        auto subscriber          = std::make_shared<Subscriber>(this);
        subscriber->subscription = impl::insert(subscriber, path, col);
        return subscriber->subscription;
    }

    void newValues(Subscription *s)
    {
        auto row_ = row(s);
        emit model->dataChanged(
                index(row_, 0), index(row_, s->size() - 1),
                {Qt::DisplayRole});
    }

  private:
    struct Subscriber : PdCom::Subscriber
    {
        Subscriber(event_impl *_impl) :
            PdCom::Subscriber {PdCom::event_mode}, impl {_impl}
        {}

        event_impl *const impl;
        Subscription *subscription;

        void stateChanged(const PdCom::Subscription &s) override
        {
            impl->stateChanged(s);
        }
        void newValues(std::chrono::nanoseconds) override
        {
            impl->newValues(subscription);
        }
    };
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct SubscriptionTableModel::poll_impl : event_impl
{
    poll_impl(SubscriptionTableModel *m, double ts) :
        event_impl {m}, sample_time {static_cast<int>(ts * 1000.0)}
    {}
    Subscription *insert(QString path, uint col) override
    {
        auto subscriber          = std::make_shared<Subscriber>(this);
        subscriber->subscription = impl::insert(subscriber, path, col);
        return subscriber->subscription;
    }

    int const sample_time;

  private:
    struct Subscriber : PdCom::Subscriber
    {
        Subscriber(poll_impl *_impl) :
            PdCom::Subscriber {PdCom::poll_mode}, impl {_impl}
        {
            timer.callOnTimeout([this] { poll(); });
            timer.setSingleShot(true);
        }

        poll_impl *const impl;
        Subscription *subscription;
        QTimer timer;
        QElapsedTimer elapsedTimer;

        void stateChanged(const PdCom::Subscription &s) override
        {
            impl->stateChanged(s);
            if (*subscription)
                poll();
            else
                timer.stop();
        }
        void newValues(std::chrono::nanoseconds) override
        {
            impl->newValues(subscription);
            auto delay = impl->sample_time - elapsedTimer.elapsed();
            if (delay > 0)
                timer.start(delay);
            else
                poll();
        }
        void poll()
        {
            if (not *subscription)
                return;

            elapsedTimer.start();
            subscription->poll();
        }
    };
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
struct SubscriptionTableModel::stream_impl : impl
{
    stream_impl(SubscriptionTableModel *m, double ts) :
        impl {m}, m_subscriber {std::make_shared<Subscriber>(this, ts)}
    {}
    Subscription *insert(QString path, uint col) override
    {
        return impl::insert(m_subscriber, path, col);
    }

  private:
    struct Subscriber : PdCom::Subscriber
    {
        Subscriber(stream_impl *_impl, double ts) :
            PdCom::Subscriber {PdCom::Transmission::fromDouble(ts)},
            impl {_impl}
        {}

        stream_impl *const impl;

        void stateChanged(const PdCom::Subscription &s) override
        {
            impl->stateChanged(s);
        }
        void newValues(std::chrono::nanoseconds t) override
        {
            impl->m_newValues(t);
        }
    };

    void m_newValues(const std::chrono::nanoseconds &)
    {
        emit model->dataChanged(
                index(0, 0),                               //
                index(rowCount() - 1, columnCount() - 1),  //
                {Qt::DisplayRole});
    }

    const std::shared_ptr<Subscriber> m_subscriber;
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
SubscriptionTableModel::SubscriptionTableModel(
        Process *process,
        const Transmission &transmission) :
    QAbstractTableModel {process},
    p_impl {std::visit(
            detail::overloaded {
                    [=](const event_t &) -> std::unique_ptr<impl> {
                        return std::make_unique<event_impl>(this);
                    },
                    [=](const poll_t &rx) -> std::unique_ptr<impl> {
                        return std::make_unique<poll_impl>(
                                this, rx.sampleTime);
                    },
                    [=](const stream_t &rx) -> std::unique_ptr<impl> {
                        return std::make_unique<stream_impl>(
                                this, rx.sampleTime);
                    },

            },
            static_cast<const Transmission::value_type &>(transmission))}
{}

SubscriptionTableModel::~SubscriptionTableModel()
{}

/////////////////////////////////////////////////////////////////////////////
ModelIndexValue SubscriptionTableModel::insert(QString path, uint col)
{
    auto subscription = p_impl->insert(path, col);
    return p_impl->index(p_impl->row(subscription), col);
}

/////////////////////////////////////////////////////////////////////////////
int SubscriptionTableModel::columnCount(const QModelIndex &) const
{
    return p_impl->columnCount();
}

/////////////////////////////////////////////////////////////////////////////
QVariant SubscriptionTableModel::data(const QModelIndex &idx, int role) const
{
    return p_impl->data(idx, role);
}

/////////////////////////////////////////////////////////////////////////////
Qt::ItemFlags SubscriptionTableModel::flags(const QModelIndex &idx) const
{
    return p_impl->flags(idx);
}

/////////////////////////////////////////////////////////////////////////////
QModelIndex
SubscriptionTableModel::index(int row, int col, const QModelIndex &idx) const
{
    return not idx.isValid() and col >= 0 and row >= 0
            ? p_impl->index(row, col)
            : QModelIndex {};
}

/////////////////////////////////////////////////////////////////////////////
int SubscriptionTableModel::rowCount(const QModelIndex &idx) const
{
    return idx.isValid() ? 0 : p_impl->rowCount();
}

/////////////////////////////////////////////////////////////////////////////
bool SubscriptionTableModel::eventFilter(QObject *obj, QEvent *ev)
{
    switch (ev->type()) {
        case QEvent::Hide:
            p_impl->unsubscribe();
            break;

        case QEvent::Show:
            p_impl->subscribe();
            break;

        default:
            break;
    }

    return QObject::eventFilter(obj, ev);
}
