/****************************************************************************
 *
 * Copyright (C) 2025 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "../include/qtpdcomtk/Q_ButtonGroup.h"

#include <QAbstractButton>
#include <QButtonGroup>
#include <QDebug>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
Q_ButtonGroup::Q_ButtonGroup(QButtonGroup *parent) : Q_Object {parent}
{
    connect(parent, &QButtonGroup::idClicked,  //
            this, &Q_ButtonGroup::buttonClicked);
}

/////////////////////////////////////////////////////////////////////////////
void Q_ButtonGroup::valueChanged(const std::chrono::nanoseconds &t)
{
    auto *group  = static_cast<QButtonGroup *>(parent());
    auto *button = group->checkedButton();

    if (group->exclusive()) {
        button = group->checkedButton();
        if (button)
            button->setChecked(false);
    }

    button = group->button(getValue(t).toInt());
    if (button)
        button->setChecked(true);
}

/////////////////////////////////////////////////////////////////////////////
void Q_ButtonGroup::buttonClicked(int i)
{
    setValue(i);
}
