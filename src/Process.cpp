/****************************************************************************
 *
 * Copyright (C) 2023 Richard Hacker <lerichi@gmx.net>
 *
 * This file is part of the QtPdComTk library.
 *
 * The QtPdComTk library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdComTk library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdComTk Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "qtpdcomtk/Process.h"

// other includes
#include "private/Process.h"

// System includes
#include <QFile>
#include <QHostInfo>
#include <exception>

using namespace qtpdcomtk;

/////////////////////////////////////////////////////////////////////////////
// qtpdcomtk::Process definitions
/////////////////////////////////////////////////////////////////////////////
Process::Process(QObject *object) :
    QObject {object}, p_impl {_private::Process::create(this)}
{
    connect(this, &Process::connected,  //
            [this] { p_impl->m_connected(); });
}
Process::~Process()
{
    setIODevice(nullptr);
}

/////////////////////////////////////////////////////////////////////////////
void Process::setIODevice(QIODevice *device)
{
    p_impl->setIODevice(device);
}

/////////////////////////////////////////////////////////////////////////////
QString Process::setMessageFile(QString path)
{
    QFile file(path);
    if (not file.open(QIODevice::ReadOnly))
        return tr("Could not open EtherLabMessages file: %1")
                .arg(file.errorString());

    QDomDocument doc("EtherLabMessages");
    QString errorMsg;
    int errorLine, errorColumn;
    if (not doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
        return tr("Failed to parse EtherLabMessages XML file on line %1, "
                  "column %2: %3")
                .arg(errorLine)
                .arg(errorColumn)
                .arg(errorMsg);

    p_impl->setMessageFile(doc);

    return {};
}

/////////////////////////////////////////////////////////////////////////////
void Process::setLanguage(QString lang)
{
    p_impl->setLanguage(lang);
}

/////////////////////////////////////////////////////////////////////////////
void Process::reset()
{
    p_impl->m_disconnected();
    PdCom::Process::reset();
    emit disconnected();
}

/////////////////////////////////////////////////////////////////////////////
bool Process::isConnected() const
{
    return p_impl->connected;
}

/////////////////////////////////////////////////////////////////////////////
std::string Process::hostname() const
{
    return QHostInfo::localHostName().toStdString();
}

/////////////////////////////////////////////////////////////////////////////
void Process::listReply(
        std::vector<PdCom::Variable> variables,
        std::vector<std::string> dirs)
{
    p_impl->listReply(variables, dirs);
}

/////////////////////////////////////////////////////////////////////////////
int Process::read(char *buf, int count)
{
    auto n = p_impl->ioDevice()->read(buf, count);
    if (n <= 0) {
        p_impl->m_disconnected();
        PdCom::Process::reset();
        emit disconnected();
    }
    return n;
}

/////////////////////////////////////////////////////////////////////////////
void Process::write(const char *buf, size_t count)
{
    p_impl->outcache.append(buf, count);
}

/////////////////////////////////////////////////////////////////////////////
void Process::flush()
{
    auto n = p_impl->ioDevice()->write(
            p_impl->outcache.data(), p_impl->outcache.size());

    if (p_impl->outcache.size() - n) {
        emit protocolError(
                tr("Could not flush entire output buffer to stream."));
        p_impl->disconnectFromHost();
    }

    p_impl->outcache.clear();
}
